import { Component, ElementRef, Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Config } from "../../util/config";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
@Component({
    selector: 'dialog-chat-room',
    templateUrl: 'dialog-chat-room.html',
    styleUrls: ['./dialog-chat-room.component.css']
})
export class DialogChatRoom {
    chatForm: FormGroup;
    constructor(private formBuilder: FormBuilder, public dialogRef: MatDialogRef<string>, @Inject(MAT_DIALOG_DATA) public data: string) {
        this.chatForm = this.formBuilder.group({
            groupName: [null, [Validators.required, Validators.minLength(3)]],
            email: [null, [Validators.required, Validators.pattern(Config.emailRegx)]]
        });
    }

    onNoClick(): void {
        this.dialogRef.close();
    }
    submit() {
        if (!this.chatForm.valid) {
            return;
        }

        /* console.log("Form data ", this.meetingForm.value);
        if (!this.meetingForm.valid) {
          return;
        }
        var name = this.meetingForm.get("name").value; */
        console.log("Form data ", this.chatForm.get("email").value);
        this.dialogRef.close({ email: this.chatForm.get("email").value, groupName: this.chatForm.get("groupName").value });
    }
}

