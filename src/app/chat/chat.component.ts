import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { ChatModel } from '../model/chat.model';
import { UploadFileService } from '../services/upload-file.service';
import { YuWeeService } from '../services/yuwee.service';
import { HttpEventType, HttpResponse } from '@angular/common/http';
@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css'],
})
export class ChatComponent implements OnInit, OnDestroy {
  isGroupMessage = true;
  msger_form_div = "msger-form-div-call";
  file_selected_div = "file-selected-div-call"
  fileName: string
  selectedFiles: FileList;
  progress = 0;
  message = '';
  addFiles = false;
  fileInfos: Observable<any>;
  // conversationList: any;
  getUserInfoListener: Subscription;
  onGeneralEventEmitter: Subscription;
  chatRoomId: string;
  messageList: any
  localUser: any;
  chatForm: FormGroup;
  @ViewChild('messageDiv') private messageDiv: ElementRef;
  @ViewChild('uploadImageInput') uploadImageInput: ElementRef<HTMLInputElement>;
  constructor(private yuweeService: YuWeeService, private formBuilder: FormBuilder, private uploadService: UploadFileService) {
    this.chatForm = this.formBuilder.group({
      message: [null, [Validators.required]]

    });
  }

  ngOnInit(): void {
    this.addFiles = false;
    this.fileInfos = this.uploadService.getFiles();
    this.localUser = {
      user: {}
    }
    this.onGeneralEventEmitter = this.yuweeService.onGeneralEventEmitter.subscribe(data => {
      switch (data.type) {
        case "registered":
          this.msger_form_div = "msger-form-div-call";
          this.file_selected_div = "file-selected-div-call"
          this.chatRoomId = data.data.callData.roomId;
          if (data.data.sessionTokenInfo && data.data.sessionTokenInfo.user) {
            this.localUser.user = data.data.sessionTokenInfo.user
          } else {
            this.localUser.user = this.getUser();
          }
          this.getAllChats();
          break;
        case "getAllChats":
          this.msger_form_div = "msger-form-div-chat";
          this.file_selected_div = "file-selected-div-chat"
          this.chatRoomId = data.roomId;
          this.localUser.user = this.getUser();
          this.getAllChats();
          break;
        case "conversation":
          this.isGroupMessage = data.data.result ? data.data.result.isGroup : false;
          this.messageList = data.data.result.messages;
          this.checkMessages();
          console.log("messageList", this.messageList);
          break;
        case "message_receive":
          if (this.chatRoomId === data.message.roomId) {
            this.messageList.push(data.message)
            if (data.message.messageType === "FILE" && data.message.fileInfo._id) {
              this.getFileUrl(data.message.fileInfo._id);
            }
            this.markAsRead(this.chatRoomId);
          }
          break;
        case "message_send":
          this.messageList.push(data.message);
          if (data.message.messageType === "FILE" && data.message.fileInfo._id) {
            this.getFileUrl(data.message.fileInfo._id);
          }
          break;
        case "deletionSuccess":
          this.deleteMessageFromList(data.data.messageId);
          break;
        case "deletionFailed":
          break;
        case "deletedBySender":
          console.log("deletedBySender", data);
          if (data.data.group && data.data.group.roomId === this.chatRoomId) {
            this.deleteMessageFromList(data.data.messageId);
          }
          break;
      }

      /* setTimeout(() => {
        this.scrollToBottom();
      }, 200); */
    });
  }

  deleteMessageFromList(messageId) {
    for (var i = 0; i < this.messageList.length; i++) {
      if (this.messageList[i].messageId === messageId) {
        this.messageList.splice(i, 1);
        break;
      }
    }
  }
  onFocusEvent(event: any) { // without type info
    console.log(event.target.value);
    this.yuweeService.getYuWeeClient().sendTypingStatus(this.chatRoomId);
  }
  onBlurEvent(event: any) {
    console.log(event.target.value);
  }
  markAsRead(roomId) {
    this.yuweeService.getYuWeeClient().markMessagesReadInRoom(roomId)
  }

  getUser() {
    try {
      var data = localStorage.getItem('yuvitimeStorage');
      var temp = JSON.parse(data).user
      return temp;
    } catch (err) {
      return {};
    }
  }
  clickToUpload() {
    this.uploadImageInput.nativeElement.value = null;
    this.uploadImageInput.nativeElement.click();
  }
  checkMessages() {
    for (var index = 0; index < this.messageList.length; index++) {
      if (this.messageList[index].messageType === "FILE") {
        this.getFileUrl(this.messageList[index].fileInfo._id);
      }
    }
  }
  getFileUrl(fileId) {
    this.yuweeService.getYuWeeClient().getFileUrl(fileId, data => {
      console.log("checkFileAndGetUrl", data);
      if (data && data.status === "success") {
        this.putURLToList(data.result);
      }
    });
  }
  putURLToList(fileDetails) {
    if (fileDetails && fileDetails.fileId) {
      for (var index in this.messageList) {
        if (this.messageList[index].messageType === "FILE" && this.messageList[index].fileInfo._id === fileDetails.fileId) {
          this.messageList[index].fileInfo.url = fileDetails.path;
          break;
        }
      }
    }

  }

  deleteForAll(message) {
    this.yuweeService.getYuWeeClient().deleteMessageInRoom(this.chatRoomId, message.messageId, "FOR_ALL");
  }
  deleteForMe(message) {
    this.yuweeService.getYuWeeClient().deleteMessageInRoom(this.chatRoomId, message.messageId, "FOR_ME");
  }
  getSrc(message) {
    if (this.isGroupMessage) {
      return "tick_send.svg";
    } else if (message && message.receiverInfo && message.receiverInfo.length === 1) {
      if (message.receiverInfo[0]._id === this.localUser.user._id) {
        return "";
      } else {
        if (message.receiverInfo[0].hasRead === 1) {
          return "tick_seen.svg";
        } else if (message && (!message.receiverInfo || message.receiverInfo.length === 0)) {
          return "tick_send.svg";
        } else {
          return "tick_receive.svg";
        }
      }

    } else {
      return "tick_send.svg";
    }
  }
  scrollToBottom(): void {
    try {
      this.messageDiv.nativeElement.scrollTop = this.messageDiv.nativeElement.scrollHeight;
    } catch (err) { }
  }
  uploadFile(): void {
    this.progress = 0;
    var currentFile = this.selectedFiles.item(0);
    this.closeFileUplaod();
    this.yuweeService.getYuWeeClient().upload(this.chatRoomId, currentFile, (data) => {
      console.log("uploadupload", data);

      if (data && data.status === "success") {

      } else if (data && data.status === "uploading") {

      }
    });
    /* this.uploadService.upload(this.currentFile, this.roomId).subscribe(
      event => {
        if (event.type === HttpEventType.UploadProgress) {
          this.progress = Math.round(100 * event.loaded / event.total);
          console.log("progress",  this.progress);
          
        } else if (event instanceof HttpResponse) {
          this.message = event.body.message;
          this.fileInfos = this.uploadService.getFiles();
          console.log("message: " + this.message);
          
        }
      },
      err => {
        this.progress = 0;
        this.message = 'Could not upload the file!';
        this.currentFile = undefined;
      }); */
    //this.selectedFiles = undefined;
  }
  downloadFile(url, name) {
    console.log(url);
    //url = "https://images.pexels.com/photos/1591447/pexels-photo-1591447.jpeg";
    //url = "https://file-examples-com.github.io/uploads/2017/02/file_example_XLS_10.xls";
    window.open(url, '_blank');
    /* var link = document.createElement("a");
      link.download = name;
      link.href = url;
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
      link.remove(); */
  }
  closeFileUplaod() {
    this.addFiles = false;
    this.selectedFiles = undefined;
    this.fileName = "";
  }
  selectFile(event): void {
    this.selectedFiles = event.target.files;
    if (this.selectedFiles && this.selectedFiles.length > 0) {
      //this.addFiles = true;
      //this.fileName = this.selectedFiles.item(0).name;
      // console.log(this.selectedFiles);
      //console.log(this.selectedFiles.item(0).name);

      this.fileName = this.selectedFiles.item(0).name;
      this.uploadFile();
    }



  }
  getDateTime(dateString) {
    return new Date(dateString).toLocaleString();
  }
  submit() {
    if (!this.chatForm.valid) {
      return;
    }
    console.log(this.chatForm.value);
    if (this.chatForm.get('message').value && this.chatForm.get('message').value.length > 0) {
      this.sendMessage(this.chatForm.get('message').value)
    }
  }
  sendMessage(message) {
    this.yuweeService.getYuWeeClient().sendMessage(this.chatRoomId, message, "My Chat");
    this.chatForm.reset();
  }

  getAllChats() {
    this.yuweeService.getYuWeeClient().fetchChatMessages(this.chatRoomId);
  }
  ngOnDestroy(): void {
    if (this.getUserInfoListener) {
      this.getUserInfoListener.unsubscribe();
    }
    if (this.onGeneralEventEmitter) {
      this.onGeneralEventEmitter.unsubscribe();
    }
  }


}
