import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent, DialogNewMeeting } from './dashboard/dashboard.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {YuWeeMaterialModule} from "./material-module";
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { RecaptchaModule, RecaptchaFormsModule  } from 'ng-recaptcha';
import {HttpClientModule} from '@angular/common/http';

import {MatNativeDateModule} from '@angular/material/core';
import { DialogJoinMeeting, MeetingComponent } from './meeting/meeting.component';
import { SDKLoginComponent } from './sdklogin/sdklogin.component';
import { SDKSignupComponent } from './sdksignup/sdksignup.component';
import { ChatComponent } from './chat/chat.component';
import { SoundWaveLoaderComponent } from './sound-wave-loader/sound-wave-loader.component';
import { ParticipantsComponent } from './participants/participants.component';
import { YuWeeService } from './services/yuwee.service';
import { CoreModule } from './core.module';
import { ConversationComponent } from './conversation/conversation.component';
import { RouterModule } from '@angular/router';
import { DialogChatRoom } from './dialog/chatroom/dialogchatroom.component';
import { SearchComponent } from './search/search.component';
import { ListFilterPipe } from './conversation/list-filter.pipe';
// import { HighlightModule, HIGHLIGHT_OPTIONS } from 'ngx-highlightjs';
// import { HighlightJsModule } from 'ngx-highlight-js';
//import { ToastrModule } from 'ngx-toastr';


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    MeetingComponent,
    DialogJoinMeeting,
    SDKLoginComponent,
    SDKSignupComponent,
    ChatComponent,
    SoundWaveLoaderComponent,
    ParticipantsComponent,
    ConversationComponent,
    DialogChatRoom,
    SearchComponent,
    ListFilterPipe,
    DialogNewMeeting
    
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    BrowserAnimationsModule,
    YuWeeMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    MatNativeDateModule,
    NgxIntlTelInputModule,
    RecaptchaModule, 
    RecaptchaFormsModule,
    CoreModule
    //ToastrModule
  ],
  providers: [YuWeeService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
