import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Config } from '../util/config';
import { YuWeeService } from '../services/yuwee.service';
import { Subscription } from 'rxjs';
import { CommonService } from '../services/common.service';
import { MustMatch } from '../helper/must.match.validator';


@Component({
  selector: 'app-sdksignup',
  templateUrl: './sdksignup.component.html',
  styleUrls: ['./sdksignup.component.css']
})


export class SDKSignupComponent implements OnInit, OnDestroy {
  signupForm: FormGroup;
  getUserInfoListener: Subscription;
  onAccountCreation: Subscription;
  siteKey = Config.siteKey;
  constructor(private router: Router, private commonService: CommonService, private yuweeService: YuWeeService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.gotoDashboard();
    this.signupForm = this.formBuilder.group({
      name: [null, [Validators.required, Validators.minLength(3)]],
      email: [null, [Validators.required, Validators.pattern(Config.emailRegx)]],
      password: [null, [Validators.required, Validators.minLength(6), Validators.maxLength(20)]],
      confirmPassword: [null, [Validators.required, Validators.minLength(6), Validators.maxLength(20)]],
    //  captcha: [null, [Validators.required]],

    }, {
      validator: MustMatch('password', 'confirmPassword')
    });


    this.getUserInfoListener = this.yuweeService.getUserInfoListener.subscribe(data => {
      localStorage.setItem("yuvitimeStorage", JSON.stringify(data));
      this.gotoDashboard();
    });
    this.onAccountCreation = this.yuweeService.onAccountCreation.subscribe(data=>{
      if(data.status === "success"){
        console.log("Successfully created");
        this.gotoSignIn();
      }else{
        console.log("Not created");
      }
    });
  }
  gotoDashboard() {
    if (this.commonService.isLoggedIn()) {
      this.router.navigate(['dashboard'])
    }
  }
  gotoSignIn(){
    this.router.navigate(['sdklogin']);
  }

  ngOnDestroy(): void {
    if (this.getUserInfoListener) {
      this.getUserInfoListener.unsubscribe();
    }
    if(this.onAccountCreation){
      this.onAccountCreation.unsubscribe();
    }
  }
  submit() {
    if (!this.signupForm.valid) {
      console.log("Form Data", this.signupForm.value);
      
      return;
    }
   
    this.yuweeService.createUser( this.signupForm.get("name").value, this.signupForm.get("email").value, this.signupForm.get("password").value);
  }
  public resolved(captchaResponse: string) {
    console.log(`Resolved captcha with response: ${captchaResponse}`);
  }
}
