import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SDKSignupComponent } from './sdksignup.component';

describe('SDKSignupComponent', () => {
  let component: SDKSignupComponent;
  let fixture: ComponentFixture<SDKSignupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SDKSignupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SDKSignupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
