export const localConfig = {
    keys: "aaaaaaaaaaaaaaaa",
    env: 'local',
    emailRegx: /^(([^<>+()\[\]\\.,;:\s@"-#$%&=]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,3}))$/,
    appUrl: "http://localhost:4200/",
    baseUrl: "",
    checkAvailability: "",
    individualSignup: "",
    otherSignup: "",

    getSubscriptionPlans: "",
    getFileAccessUrl: "",
    individualLogin: "",

    

    ///////////////Keys for SDK////////////////////////////////
    callClientId: "",
    callAppId: "",
    callAppSecret: "",

  
    siteKey: "" //
}

export const prodConfig = {
    keys: "aaaaaaaaaaaaaaaa",
    env: 'production',
    emailRegx: /^(([^<>+()\[\]\\.,;:\s@"-#$%&=]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,3}))$/,
    appUrl: "",
    baseUrl: "",
    checkAvailability: "",
    individualSignup: "",
    otherSignup: "",

    getSubscriptionPlans: "",
    getFileAccessUrl: "",
    individualLogin: "",
   
    ///////////////Keys for SDK////////////////////////////////
    callClientId: "",
    callAppId: "",
    callAppSecret: "",
    //////////////////////////////////////////////////////////////


    siteKey: "",
}