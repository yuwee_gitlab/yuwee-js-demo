export enum State {
    TILE_GRID = "tile",
    SPOTLIGHT = "spotlight",
    SCREEN_SHARE = "ScreenShare",
  }