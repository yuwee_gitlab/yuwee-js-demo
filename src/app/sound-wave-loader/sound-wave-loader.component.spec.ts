import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SoundWaveLoaderComponent } from './sound-wave-loader.component';

describe('SoundWaveLoaderComponent', () => {
  let component: SoundWaveLoaderComponent;
  let fixture: ComponentFixture<SoundWaveLoaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SoundWaveLoaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SoundWaveLoaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
