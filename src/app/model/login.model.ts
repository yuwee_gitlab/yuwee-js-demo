

interface Notification {
    email: boolean;
}

interface User {
    _id: string;
    dateOfModification: number;
    dateOfCreation: number;
    mobile: string;
    name: string;
    email: string;
    image: string;
    accountId: string;
    facebookId?: any;
    googleId?: any;
    linkedInId?: any;
    status: number;
    customStatus: string;
    userType: string;
    notification: Notification;
    privateKey: string;
    isDev: boolean;
    accountType: string;
    accountName: string;
    isWL: boolean;
    awsBucketName: string;
    fileUploadTarget: string;
}

interface Result {
    user: User;
}

export interface LoginModel {
    access_token: string;
    refresh_token: string;
    expiresIn: number;
    message: string;
    status: string;
    result: Result;
    token_type: string;
}


