

interface Result {
    meetingTokenId: number;
    meetingName: string;
    passcode: string;
    maxAllowedParticipant: number;
    isCallAllowedWithoutInitiator: boolean;
    accessType: string;
    members: string[];
    presenters: string[];
    callAdmins: string[];
    callMode: number;
    meetingExpireDuration: number;
    meetingStartTime: number;
    subPresenterPasscode: string;
    presenterPasscode: string;
}

export interface HostMeeting {
    message: string;
    status: string;
    result: Result;
}



