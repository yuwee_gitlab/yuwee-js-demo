
export interface JoinUserModel {
    _id: string;
    email: string;
    name: string;
    image: string;
    isAudioOn: boolean;
    isVideoOn: boolean;
    isCallAdmin: boolean;
    isPresenter: boolean;
    isSubPresenter: boolean;
    status: string;
    videoIconURL:string;
    audioIconURL: string;

}

