export class SubcriptionPlan{
    message: string;
    status: string;
    result: {
        data: Array<Plan>;
    };
}
class Plan{
    planName: string;
}