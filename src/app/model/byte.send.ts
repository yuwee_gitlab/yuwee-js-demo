export interface BytesSent{
    pAudio: number;
    pVideo: number;
    audioByte: string;
    videoByte: string;
}