export interface MeetingJoinModel {
  meetingId: string;
  name: string;
  email: string;
  presenterPasscode: string
  subPresenterPasscode: string
  viewerPasscode: string;
  role: string;
}