export interface StreamModel{
    id: string;
    stream: any;
    role: string;
    userId: string;
}