

 interface Sender {
    senderId: string;
    name: string;
    image: string;
    email: string;
}

 interface Group {
    groupId: string;
    name: string;
    image: string;
    groupAdmins: string[];
}

 interface Receiver {
    _id: string;
    hasLeft: boolean;
    name: string;
    email: string;
    image: string;
    isOnline: boolean;
}

export interface ChatModel {
    messageId: string;
    roomId: string;
    message: string;
    isGroup: boolean;
    destinationId: string;
    sender: Sender;
    group: Group;
    messageType: string;
    receivers: Receiver[];
    dateOfCreation: number;
    purpose?: any;
    busyUserList: any[];
    quotedMessage?: any;
    isEncrypted: boolean;
    color: string;
    time: string;
}



