import { Component, OnDestroy, OnInit, Inject, ViewChild, ElementRef, AfterViewInit, ChangeDetectorRef, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { map } from 'rxjs/operators';
import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';
import { Observable, Subscription } from 'rxjs';
import { MeetingJoinModel } from '../model/meeting-dialog-model';
import { YuWeeService } from '../services/yuwee.service';
import { CommonService } from '../services/common.service';
import { ParticipantsService } from '../services/participants.service';
import { StreamModel } from '../model/stream.model';
import { State } from '../util/state.enum';
import { BytesSent } from '../model/byte.send';

@Component({
  selector: 'app-meeting',
  templateUrl: './meeting.component.html',
  styleUrls: ['./meeting.component.css']
})
export class MeetingComponent implements AfterViewInit, OnInit, OnDestroy {
  localStreamReceiver: Subscription;
  onRemoteStream: Subscription;
  getUserInfoListener: Subscription;
  meetingJoinModel: MeetingJoinModel;
  streamModel: StreamModel;
  leaveMeetingListener: Subscription;
  onGeneralEventEmitter: Subscription;
  onParticipantsChanged: Subscription;
  onOneToOneCallListner: Subscription;
  userStreamMap = new Map<string, StreamModel>();
  yuweeVersion: string;
  sharingUserByteId: string;
  speakerName: string;
  isSpotlightEnabled = false;
  isTileEnabled = true;
  newMessage = false;
  chatOpen = false;
  newMessageData: any;
  newMessageTimer = null;
  resolutionArray = ["cif", "qcif", "sif", "svga", "hvga", "hd720p"];
  currentResolution = "hd720p";
  updatingResolution = null;
  CALL_TYPE: string;
  EXECUTION_TYPE: string;
  CALL_ID: string;
  CALL_MEDIA_TYPE: string;
  byteSent: BytesSent = {
    pAudio: 0,
    pVideo: 0,
    audioByte: '0',
    videoByte: '0'
  };
  sharingUserByte: BytesSent = {
    pAudio: 0,
    pVideo: 0,
    audioByte: '0',
    videoByte: '0'
  };
  sharingVideoByte: BytesSent = {
    pAudio: 0,
    pVideo: 0,
    audioByte: '0',
    videoByte: '0'
  };
  sharingUserId: string;
  localUser: any = {};
  oldTime = 0;
  currentSpearker = "";
  horizontalPosition: MatSnackBarHorizontalPosition = 'right';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  @ViewChild('ivMore') ivMore: ElementRef<HTMLImageElement>;
  @ViewChild('popup') popup: ElementRef<HTMLDivElement>;
  @ViewChild('gridVideo', { static: true }) gridVideo: ElementRef<HTMLDivElement>;
  @ViewChild('spotlight', { static: true }) spotlight: ElementRef<HTMLDivElement>;
  @ViewChild('localvideo') localvideo: ElementRef<HTMLVideoElement>;
  @ViewChild('sharedUservideo') sharedUservideo: ElementRef<HTMLVideoElement>;
  @ViewChild('shareVideoDiv') shareVideoDiv: ElementRef<HTMLDivElement>;
  @ViewChild('shareSelfView') shareSelfView: ElementRef<HTMLDivElement>;
  toggleShareImage = "monitor-share.svg";
  reconnecting_text = null;
  reconnecting = false;
  fromDisconnected = false;
  participantsCounter = 0;
  columns = 1;
  rowHeight = "2.4:1";
  presenterList = [];
  subPresenterList = [];
  presenterCounter = 0;
  isRegistered = false;
  connecting = true;
  askJoin = false;
  registerMeegingData;
  joinName = "";
  leftName = "";
  joinTimer = null;
  leftTimer = null;


  localAudioVideoIconSelection = {
    audio: "mic-white-18dp.svg",
    video: "videocam-white-18dp.svg",
    hand: "hand-normal.svg"
  };
  localMediaActionAPI = {
    audio: false,
    video: false
  }
  localMediaStateSelection = {
    audio: true,
    video: true,
    shareScreen: false,
    handRaise: false,
  }

  constructor( private snackBar: MatSnackBar, private participantsService: ParticipantsService, private cdr: ChangeDetectorRef, private location: Location, private commonService: CommonService, private router: Router, private activatedRoute: ActivatedRoute, private yuweeService: YuWeeService, public dialog: MatDialog) {

  }

  ngAfterViewInit() {
    console.log("gridVideo", this.gridVideo);
    //this.tileUpdate();

  }
  ngOnInit(): void {
    this.addListener();
    var data = localStorage.getItem("userData");
    if (data) {
      var userData = JSON.parse(data)
      this.yuweeService.createUserSession(userData.email, userData.password);
    } else {
      this.afterSessionCreated();
    }
    this.getUserInfoListener = this.yuweeService.getUserInfoListener.subscribe(() => {
      this.afterSessionCreated();
    });

  }

  afterSessionCreated() {
    this.CALL_TYPE = this.activatedRoute.snapshot.params["type"];
    this.CALL_ID = this.activatedRoute.snapshot.params["callId"];
    this.CALL_MEDIA_TYPE = this.activatedRoute.snapshot.params["mediaType"];
    if (this.CALL_TYPE === "onetoone") {
      this.startOneToOneCall();
      return;
    }
    this.currentResolution = "hd720p";
    var meetingId: string;
    var name: string;
    var email: string;
    var presenterPasscode: string
    var subPresenterPasscode: string
    var viewerPasscode: string
    try {
      meetingId = this.activatedRoute.snapshot.queryParams["meetingId"];
      this.EXECUTION_TYPE = this.activatedRoute.snapshot.queryParams["type"] ? this.activatedRoute.snapshot.queryParams["type"] : 'normaml';
      if (this.activatedRoute.snapshot.queryParams["pq"]) {
        var code = this.activatedRoute.snapshot.queryParams["pq"];
        var decoded = atob(code);
        var array = decoded.split('apvpa');

        presenterPasscode = this.commonService.decrypt(decodeURIComponent(array[0]));
        subPresenterPasscode = this.commonService.decrypt(decodeURIComponent(array[1]));
        viewerPasscode = this.commonService.decrypt(decodeURIComponent(array[2]));
        //console.log("presenterPasscode", presenterPasscode);
        //console.log("subPresenterPasscode", subPresenterPasscode);
        //console.log("viewerPasscode", viewerPasscode);
      }
      var yuvitimeStorage = localStorage.getItem("yuvitimeStorage");
      var userData;
      if (yuvitimeStorage) {
        userData = JSON.parse(yuvitimeStorage);
        name = userData.user.name;
        email = userData.user.email;
      } else if (!yuvitimeStorage && this.EXECUTION_TYPE === "test") {
        name = this.EXECUTION_TYPE + new Date().getTime();
        email = this.EXECUTION_TYPE + new Date().getTime() + '@' + this.EXECUTION_TYPE + ".com"
      }

      this.meetingJoinModel = {
        meetingId: meetingId,
        presenterPasscode: presenterPasscode,
        subPresenterPasscode: subPresenterPasscode,
        viewerPasscode: viewerPasscode,
        name: name,
        email: email,
        role: "subPresenter"
      }
    } catch (err) {

    }

    if (meetingId && (presenterPasscode || subPresenterPasscode || viewerPasscode) && name && email) {
      this.openDialog(this.meetingJoinModel);
    } else {
      this.openDialog(this.meetingJoinModel);
    }
  }
  startOneToOneCall() {
    this.addOneToOneLister();
    this.localUser.user = this.getUser();
    if (!this.CALL_ID) {
      var callParams = {
        isGroup: false,
        inviteeEmail: "test@test.com",
        inviteeName: "test",
        invitationMessage: "Hi test, Lets have a call",
        callType: "video"
      };
      this.yuweeService.getYuWeeClient().setupOneToOneCall(callParams);
    } else {
      var state: Observable<object>;
      state = this.activatedRoute.paramMap.pipe(map(() => window.history.state));
      state.subscribe(data => {
        var callData: any = data;
        this.yuweeService.getYuWeeClient().acceptCall(callData.callType);
        this.connecting = false;
      });
    }
  }

  addOneToOneLister() {
    this.yuweeVersion = this.yuweeService.getYuWeeClient().getVersion();
    this.onOneToOneCallListner = this.yuweeService.onOneToOneCallListner.subscribe((data) => {
      if (data && data.type === "READY_TO_INITIATE") {
        this.connecting = false;
      } else if (data.type === "callAccepted") {

      } else if (data.type === "remoteUserHangUp") {
        if (data.data && data.data.senderName) {
          this.onLeftUser(data.data.senderName);
          this.userStreamMap.clear();
        }
        this.presenterList.pop();
        this.tileUpdate();
      } else if (data.type === "remoteStream") {
        this.renderStreamOneToOne(data.data);
      }
    });
  }
  renderStreamOneToOne(data) {
    var userId = data.userId;
    if (data && data.senderName && data.streamType === "user" && !this.userStreamMap.get(userId + data.streamType)) {
      this.onJoinUser(data.senderName);
    }
    if (data.streamType === "screen") {
      this.shareVideoDiv.nativeElement.setAttribute('style', 'display: block;');
      var shareVideo = document.querySelector("#shareVideo");
      this.yuweeService.getYuWeeClient().attachMediaStream(shareVideo, data.stream);

      this.localvideo.nativeElement.parentElement.setAttribute('style', 'display:none !important');
      this.sharedUservideo.nativeElement.parentElement.setAttribute('style', 'display:block !important');
      var sharedUservideo = document.querySelector("#sharedUservideo");

      if (this.userStreamMap.get(userId + 'user')) {
        this.sharingUserByteId = userId;
        this.yuweeService.getYuWeeClient().attachMediaStream(sharedUservideo, this.userStreamMap.get(userId + 'user').stream);
      } else {
        console.log("Shared user stream not found");
      }
    } else if (data.streamType === "user" && this.sharingUserByteId && this.sharingUserByteId === data.userId) {
      this.localvideo.nativeElement.parentElement.setAttribute('style', 'display:none !important');
      this.sharedUservideo.nativeElement.parentElement.setAttribute('style', 'display:block !important');
      var sharedUservideo = document.querySelector("#sharedUservideo");
      this.yuweeService.getYuWeeClient().attachMediaStream(sharedUservideo, data.stream);

    }
    else if (data.streamType === "user") {
      this.shareVideoDiv.nativeElement.setAttribute('style', 'display: none;');
      this.localvideo.nativeElement.parentElement.setAttribute('style', 'display:block !important');
      this.sharedUservideo.nativeElement.parentElement.setAttribute('style', 'display:none !important');
      this.presenterList = this.remveItemFromList(this.presenterList, "videoid_" + userId);
      this.presenterList.push(
        { audioByte: 0, videoByte: 0, pAudio: 0, pVideo: 0, cols: 1, rows: 1, id: "videoid_" + userId, userId: userId }
      );
      this.tileUpdate();

      var bigVideo = document.querySelector("#videoid_" + userId);
      this.yuweeService.getYuWeeClient().attachMediaStream(bigVideo, data.stream);
    }





    this.userStreamMap.set(userId + data.streamType, {
      id: data.stream.id,
      stream: data.stream,
      role: null,
      userId: userId
    });


  }

  toggleHandRaise() {
    if (this.localMediaStateSelection.handRaise) {
      this.localMediaStateSelection.handRaise = false;
      this.localAudioVideoIconSelection.hand = "hand-normal.svg";
      this.makeHandToggle(false);
    } else {
      this.localMediaStateSelection.handRaise = true;
      this.localAudioVideoIconSelection.hand = "hand-raise.svg";
      this.makeHandToggle(true);
    }
  }
  makeHandToggle(raiseHand) {
    this.yuweeService.getYuWeeClient().toggleHandRaise(this.localUser.user._id, raiseHand, (err, data) => {
      console.log("My hand raise", err, data);

    });
  }

  updateMutedStatus(isMuted) {
    if (isMuted) {
      this.localMediaStateSelection.audio = false;
      this.localAudioVideoIconSelection.audio = "mic_off-white-18dp.svg";
    } else {
      this.localMediaStateSelection.audio = true;
      this.localAudioVideoIconSelection.audio = "mic-white-18dp.svg";
    }
  }
  toggleAudio() {
    if(this.meetingJoinModel.role === "viewer"){
      this.showSnackBar("You can't toggle audio");
      return;
    }
    if (this.localMediaActionAPI.audio) {
      return; // API is progress
    }
    if (this.localMediaStateSelection.audio) {
      this.localMediaStateSelection.audio = false;
      //this.toggleAudioImage = "mic_off-white-18dp.svg";
      this.localAudioVideoIconSelection.audio = "mic_off-white-18dp.svg";
    } else {
      this.localMediaStateSelection.audio = true;
      //this.toggleAudioImage = "mic-white-18dp.svg";
      this.localAudioVideoIconSelection.audio = "mic-white-18dp.svg";
    }
    this.localMediaActionAPI.audio = true; //API has called
    this.yuweeService.getYuWeeClient().updateAndPublishLocalStream(this.localMediaStateSelection.audio, this.localMediaStateSelection.video, (err, data) => {
      console.log("updateAndPublishLocalStream-A", data);
      this.localMediaActionAPI.audio = false; //API finish
      this.selfMute();
    });


  }
  selfMute() {
    this.yuweeService.onGeneralEventEmitter.next({
      user: {
        userId: this.localUser.user._id,
        isMuted: !this.localMediaStateSelection.audio
      },
      type: 'selfMuted'
    });
  }
 
  toggleVideo() {
    if(this.meetingJoinModel.role === "viewer"){
      this.showSnackBar("You can't toggle video");
      return;
    }
    if (this.localMediaActionAPI.video) {
      return; //////API is progress called
    }
    if (this.localMediaStateSelection.video) {
      this.localMediaStateSelection.video = false;
      //this.toggleVideoImage = "videocam_off-white-18dp.svg";
      this.localAudioVideoIconSelection.video = "videocam_off-white-18dp.svg";
    } else {
      this.localMediaStateSelection.video = true;
      //this.toggleVideoImage = "videocam-white-18dp.svg";
      this.localAudioVideoIconSelection.video = "videocam-white-18dp.svg";
    }
    this.localMediaActionAPI.video = true; //API has called
    this.yuweeService.getYuWeeClient().updateAndPublishLocalStream(this.localMediaStateSelection.audio, this.localMediaStateSelection.video, (err, data) => {
      console.log("updateAndPublishLocalStream-V", data);
      this.localMediaActionAPI.video = false;

    });
    /* this.yuweeService.getYuWeeClient().toggleVideoPause((err, data) => {
      console.log("toggleVideotoggleVideo", data);
      if (data && data.isVideoOn) {
        this.toggleVideoImage = "video-white.png";
   
      } else {
        this.toggleVideoImage = "video-paused-white.png";
      }
   
    }); */
  }
  togleScreenShare() {
    if (!this.localMediaStateSelection.shareScreen) {
      this.localMediaStateSelection.shareScreen = true;
    } else {
      this.localMediaStateSelection.shareScreen = false;
    }
    this.yuweeService.getYuWeeClient().toggleScreenShare();
  }
  getHeight(count: number): number {
    var height = this.gridVideo.nativeElement.offsetHeight;
    var rowCount = this.commonService.getRowCount(count);
    return height / rowCount;
  }
  tileUpdate() {
    this.presenterCounter = this.presenterList.length;
    this.columns = this.commonService.getColumns(this.presenterCounter);
    this.rowHeight = this.getHeight(this.presenterCounter) + "px"; //this.commonService.getRowHeight(this.columns, this.gridVideo.nativeElement);

    this.cdr.detectChanges();


  }
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    //event.target.innerWidth;
    this.rowHeight = this.getHeight(this.presenterCounter) + "px";

    this.cdr.detectChanges();
  }
  subscribeStreamAfterAvaliableStream(streamInfoObj) {
    var data = localStorage.getItem("meetingSession");
    if (this.CALL_TYPE === "onetoone") {
      //noting
    } else if (!data) {
      return;
    }
    /* var tempData = JSON.parse(data);
    var userInfo = tempData.sessionTokenInfo;
    if (!userInfo) {
      if (localStorage.getItem("yuvitimeStorage")) {
        userInfo = JSON.parse(localStorage.getItem("yuvitimeStorage"))
      }
  
    } */
    var recentSubcribedUser = streamInfoObj
    /* if (this.userStreamMap && this.userStreamMap.get(recentSubcribedUser.userId +streamInfoObj.streamType)) {
      return;
    } */

    if (this.CALL_TYPE === "onetoone" || (this.localUser && this.localUser.user && this.localUser.user._id && streamInfoObj.userId !== this.localUser.user._id)) {
      //console.log("going to subscribe stream", streamInfoObj, streamInfoObj.userId);

      this.yuweeService.getYuWeeClient().subscribeStream(streamInfoObj.userId, streamInfoObj.streamType, { audio: true, video: true }, (err, data) => {


      });
    } else if (streamInfoObj.streamType === "screen") {
      this.gridVideo.nativeElement.setAttribute("style", "display:none;");
      this.spotlight.nativeElement.setAttribute("style", "display:none");
      this.shareSelfView.nativeElement.setAttribute("style", "display:block");
      console.log("*********=====*****===============================I am sharing");
      console.log("**********==========*****==========================I am sharing the screen");
    }

  }

  renderStream(data) {
    var recentSubcribedUser = data;
    if (this.localUser && this.localUser.user && this.localUser.user._id && this.localUser.user._id === recentSubcribedUser.userId) {
      if (data.streamType === "screen") {
        this.gridVideo.nativeElement.setAttribute("style", "display:none;");
        this.spotlight.nativeElement.setAttribute("style", "display:none");
        this.shareSelfView.nativeElement.setAttribute("style", "display:block");
        console.log("**********==========*****==========================I am sharing the screen");
      }
    } else {
      if (this.fromDisconnected && this.participantsCounter >= this.userStreamMap.size) {
        this.participantsCounter = 0;
        this.fromDisconnected = false;
      }
      var oldStream = this.userStreamMap.get(recentSubcribedUser.userId + data.streamType)
      if (data.streamType === "screen") {
        this.onShareStream(data, recentSubcribedUser.userId);
        this.pushStreamToMap(recentSubcribedUser, data);
      }
      else if (this.fromDisconnected || !oldStream || oldStream.id !== data.stream.id) {
        this.participantsCounter++;
        if (recentSubcribedUser.role === "subPresenter") {///////Sub-Presenter
          this.subPresenterList = this.remveItemFromList(this.subPresenterList, "videoid_" + recentSubcribedUser.userId);
          this.subPresenterList.push(
            { audioByte: 0, videoByte: 0, pAudio: 0, pVideo: 0, id: "videoid_" + recentSubcribedUser.userId, userId: recentSubcribedUser.userId }
          );
          this.cdr.detectChanges();
          var subPresenterVideo = document.querySelector("#videoid_" + recentSubcribedUser.userId);
          this.yuweeService.getYuWeeClient().attachMediaStream(subPresenterVideo, data.stream);
        }
        else { ///////Presenter
          this.presenterList = this.remveItemFromList(this.presenterList, "videoid_" + recentSubcribedUser.userId);
          this.presenterList.push(
            { audioByte: 0, videoByte: 0, pAudio: 0, pVideo: 0, cols: 1, rows: 1, id: "videoid_" + recentSubcribedUser.userId, userId: recentSubcribedUser.userId }
          );
          this.tileUpdate();
          var bigVideo = document.querySelector("#videoid_" + recentSubcribedUser.userId);
          this.yuweeService.getYuWeeClient().attachMediaStream(bigVideo, data.stream);

          if (this.sharingUserByteId && this.sharingUserByteId === recentSubcribedUser.userId) {
            var sharedUservideo = document.querySelector("#sharedUservideo");
            this.yuweeService.getYuWeeClient().attachMediaStream(sharedUservideo, data.stream);
          }
        }
        this.pushStreamToMap(recentSubcribedUser, data);
        if (this.sharingUserByteId && this.sharingUserByteId === recentSubcribedUser.userId) {
          this.updateSharingUserVideoStream(recentSubcribedUser.userId);
        }

      }
    }

  }
  onShareStream(data, userId) {
    this.shareVideoDiv.nativeElement.setAttribute('style', 'display: block;');
    var shareVideo = document.querySelector("#shareVideo");
    this.yuweeService.getYuWeeClient().attachMediaStream(shareVideo, data.stream);

    this.localvideo.nativeElement.parentElement.setAttribute('style', 'display:none !important');
    this.sharedUservideo.nativeElement.parentElement.setAttribute('style', 'display:block !important');
    var sharedUservideo = document.querySelector("#sharedUservideo");
    if (this.userStreamMap.get(userId + 'user')) {
      this.sharingUserByteId = userId;
      this.yuweeService.getYuWeeClient().attachMediaStream(sharedUservideo, this.userStreamMap.get(userId + 'user').stream);
    } else {
      console.log("Shared user stream not found");
    }
  }
  pushStreamToMap(recentSubcribedUser, data) {
    this.userStreamMap.set(recentSubcribedUser.userId + data.streamType, {
      id: data.stream.id,
      stream: data.stream,
      role: recentSubcribedUser.role,
      userId: recentSubcribedUser.userId
    });
  }
  remveItemFromList(array, videoId) {
    array = array.filter(function (obj) {
      return obj.id !== videoId;
    });
    return array;
  }
  updateSharingUserVideoStream(userId) {
    var sharedUservideo = document.querySelector("#sharedUservideo");
    if (this.userStreamMap.get(userId + 'user')) {
      this.yuweeService.getYuWeeClient().attachMediaStream(sharedUservideo, this.userStreamMap.get(userId + 'user').stream);
    } else {
    }
  }


  leaveMeeting() {
    if (this.CALL_TYPE === "onetoone") {
      this.yuweeService.getYuWeeClient().hangupCall('end', (data) => {
        console.log("hangupCall", data);;
        this.userStreamMap.clear();

      })
    } else {
      this.yuweeService.getYuWeeClient().leaveMeeting();
    }
  }
  registerForMeeting() {
    var passcode = "";
    if (this.meetingJoinModel.role === "presenter") {
      passcode = this.meetingJoinModel.presenterPasscode;
    } else if (this.meetingJoinModel.role === "subPresenter") {
      passcode = this.meetingJoinModel.subPresenterPasscode;
    } else if (this.meetingJoinModel.role === "viewer") {
      passcode = this.meetingJoinModel.viewerPasscode;
    }
    var registerParams = {
      nickName: this.meetingJoinModel.name,
      meetingTokenId: this.meetingJoinModel.meetingId,
      passcode: passcode,
      guestId: this.meetingJoinModel.email,
      joinMedia: { "audio": true, "video": true }
    };

    this.yuweeService.getYuWeeClient().registerInMeeting(registerParams, (err, data) => {
      if (err) {
        console.log("RegisterForMeeting err", err);
      } else {
        console.log("RegisterForMeeting success", data);
        if (data) {
          this.isRegistered = true;
          localStorage.setItem("meetingSession", JSON.stringify(data));
          this.localUser = {
            isCallAdmin: data.isCallAdmin,
            isDiscussion: data.isDiscussion,
            isPresenter: data.isPresenter,
            role: data.role,
            user: data.sessionTokenInfo ? data.sessionTokenInfo.user : {}
          }

          if (!this.localUser.user || !this.localUser.user._id) {
            this.localUser.user = this.getUser();
          }
          this.registerMeegingData = data;
          if (this.EXECUTION_TYPE === "test") {
            this.localMediaStateSelection.audio = true;
            this.localMediaStateSelection.video = true;
            this.joinAfterSelections();
          } else {
            this.askJoin = true;
          }

        }
      }
    });
  }

  getUser() {
    try {
      var data = localStorage.getItem('yuvitimeStorage');
      var temp = JSON.parse(data).user
      return temp;
    } catch (err) {
      return {};
    }
  }

  /**
   * Join meeting after register
   */
  joinMeeging(data: any) {
    var joinParams = {
      icsToken: data.callTokenInfo.token,
      ICSCallResourceId: data.callTokenInfo.ICSCallResourceId,
      callId: data.callData.callId,
      roomId: data.callData.roomId,
      callMode: data.callData.callMode,
      tokenInfo: data.sessionTokenInfo,
      role: data.role
    };
    this.yuweeService.getYuWeeClient().joinMeeting(joinParams, (err, data) => {
      if (err) {
        console.log("Join Meeting Failed", err);
      } else {
        this.connecting = false;
        console.log("Join Meeting success", data);
        this.yuweeService.onGeneralEventEmitter.next({ type: "registered", "data": this.registerMeegingData });
        this.registerMeegingData = null;
        this.yuweeVersion = this.yuweeService.getYuWeeClient().getVersion();
      }

    });
  }
  ngOnDestroy(): void {
    if (this.onOneToOneCallListner) {
      this.onOneToOneCallListner.unsubscribe();
    }
    if (this.onParticipantsChanged) {
      this.onParticipantsChanged.unsubscribe();
    }
    if (this.onGeneralEventEmitter) {
      this.onGeneralEventEmitter.unsubscribe();
    }

    if (this.localStreamReceiver) {
      this.localStreamReceiver.unsubscribe();
    }
    if (this.onRemoteStream) {
      this.onRemoteStream.unsubscribe();
    }
    if (this.getUserInfoListener) {
      this.getUserInfoListener.unsubscribe();
    }
    if (this.leaveMeetingListener) {
      this.leaveMeetingListener.unsubscribe();
    }
  }

  openDialog(dataModel: MeetingJoinModel): void {
    if (this.EXECUTION_TYPE === "test") {
      this.meetingJoinModel = dataModel;
      localStorage.setItem("progressMeeting", JSON.stringify(dataModel))
      this.initMeeting();
    } else {
      const dialogRef = this.dialog.open(DialogJoinMeeting, {
        width: '400px',
        data: dataModel
      });
      dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed');
        if (!result) {
          this.router.navigate(['dashboard']);
        } else {
          this.meetingJoinModel = result;
          localStorage.setItem("progressMeeting", JSON.stringify(result))
          this.initMeeting();
        }


      });
    }

  }
  initMeeting() {
    if (!this.isRegistered) {
      this.registerForMeeting();
    }
  }

  addListener() {
    this.localStreamReceiver = this.yuweeService.localStreamReceiver.subscribe((data) => {
      var localStream = data.data;
      console.log("Data comes", localStream);
      this.yuweeService.getYuWeeClient().setActiveSpeaker(true);
      var meetingSession = localStorage.getItem("meetingSession");
      if (this.updatingResolution) {
        this.currentResolution = this.updatingResolution;
        this.updatingResolution = null;
      }
      if (data.role) {
        this.meetingJoinModel.role = data.role;
        this.yuweeService.onGeneralEventEmitter.next({
          user: {
            userId: this.localUser.user._id,
            role: data.role
          },
          type: 'myRoleUpdate'
        });
      }
      console.log("this.CALL_TYPE", this.CALL_TYPE, this.CALL_ID);
      var localUserId = "local";
      if (this.CALL_TYPE !== "onetoone") {
        var localRole = this.meetingJoinModel.role;
        if (meetingSession) {
          var tempData = JSON.parse(meetingSession);
          if (tempData) {
            localRole = tempData.role;
          }
        }
      }
      this.userStreamMap.set(localUserId, {
        id: localStream.id,
        stream: localStream,
        role: localRole,
        userId: localUserId
      });
      this.attachToLocalVideo(localStream);
      this.askJoin = false;

      if (this.registerMeegingData) {
        //this.setAudioAnalysis();
        this.joinMeeging(this.registerMeegingData);
      }
      if (data.role && data.role === "presenter") {
        // this.setAudioAnalysis();
      }
      /* if(this.isRegistered){
        
      } */

    });

    //Register listener to receivew remote stream
    //////////////////////////////////////////////
    this.onRemoteStream = this.yuweeService.onRemoteStream.subscribe((data) => {
      console.log("onRemoteStream called", data.type);
      if (data.type === "available") {
        if (data.data) {
          this.subscribeStreamAfterAvaliableStream(data.data);
        }
      } else if (data.type === "stopped") {
        this.onRemoteStreamStopped(data.data);
      } else if (data.type === "screen" && this.CALL_TYPE === "onetoone") {
        this.onShareScreenStatusChange(data.data);
      } else if (data.type === "screen") {
        this.onShareScreenStatusChange(data.data);
      } else if ('remoteStream') {
        this.renderStream(data.data);
      }

    });

    ///////////////////////////////////////

    //////////////Leave Meeting///////////
    this.leaveMeetingListener = this.yuweeService.leaveMeetingListener.subscribe(() => {
      //this.presenterList.clear();
      this.presenterList = [];
      this.tileUpdate();
      localStorage.setItem("progressMeeting", '');
      this.router.navigate(['dashboard']);
    });
    ///////////////////////////////////////

    /////////////EventEmitter////////
    this.onGeneralEventEmitter = this.yuweeService.onGeneralEventEmitter.subscribe((result) => {
      if (result && result.type) {
        this.onGeneralEventsReceived(result);
      }
    });
    //////////////////////////////

  }


  onNewMessageReceived(data) {
    if (data && data.message && data.message.sender) {
      if (data.message.sender.senderId !== this.localUser.user._id) {
        this.newMessageData = {
          name: data.message.sender.name,
          message: data.message.message
        }
        if (this.newMessageData) {
          if (this.newMessageTimer !== null) {
            clearTimeout(this.newMessageTimer);
            this.newMessageTimer = null;
          }
          this.newMessageTimer = setTimeout(() => {
            this.newMessageData = null;
            this.newMessageTimer = null;
          }, 3000);
        }
      }
    }
    if (!this.chatOpen) {
      this.newMessage = true;
    }
  }
  onGeneralEventsReceived(result) {
    switch (result.type) {
      case "join":
        this.onJoinUser(result.user.info.name);
        break;
      case "left":
        var removeData = result.user;
        removeData.userId = result.user.userId;
        removeData.streamType = "user"
        this.onRemoteStreamStopped(removeData);
        var userData = this.participantsService.getParticipantMap().get(result.user.userId);
        if (userData) {
          this.onLeftUser(userData.name);
        }
        break;
      case "audioActivity":
        var currentTime = new Date().getTime();
        var user = result.user;
        if (this.isSpotlightEnabled && this.localUser.user._id !== user.userId) {
          console.log("onAudioActivity", user.userId);
          var streamData = this.userStreamMap.get(user.userId + "user");
          if (this.currentSpearker !== user.userId && streamData && streamData.stream && streamData.role === "presenter") {
            console.log("onAudioActivity", user.userId, this.localUser.user._id);
            var spotlightVideo = document.querySelector("#spotlightVideo");
            this.yuweeService.getYuWeeClient().attachMediaStream(spotlightVideo, streamData.stream);
            this.speakerName = this.getParticipantsName(streamData.userId);
            console.log("onAudioActivity_speakerName", this.speakerName);
            this.oldTime = currentTime;
            this.currentSpearker = user.userId;
          }
        }
        break;
      case "muted":
        if (result.user.userId === this.localUser.user._id) {
          this.updateMutedStatus(result.user.isMuted);
        }
        break;
      case "toggleAudio":
        this.toggleAudio();
        break;
      case "roleUpdate":
        if(result.user.userId === this.localUser.user._id){
          var oldRole =  this.meetingJoinModel.role; 
          this.meetingJoinModel.role = result.user.newRole;
          if(this.meetingJoinModel.role === "viewer"){
            this.localMediaStateSelection.audio = false;
            this.localMediaStateSelection.video = false;
            this.localAudioVideoIconSelection.audio = "mic_off-white-18dp.svg";
            this.localAudioVideoIconSelection.video = "videocam_off-white-18dp.svg";
          }else if(oldRole === "viewer"){
            this.localMediaStateSelection.audio = true;
            this.localMediaStateSelection.video = true;
            this.localAudioVideoIconSelection.audio = "mic-white-18dp.svg";
            this.localAudioVideoIconSelection.video = "videocam-white-18dp.svg";
          }
        }
        /* if (result.user.userId !== this.localUser.user._id) {
          result.user.streamType = "user";
          this.onRemoteStreamStopped(result.user);
        } */
        break;
      case "message_receive":
        this.onNewMessageReceived(result);
        break;
      case "analytics":
        if (result.data.user === this.localUser.user._id) {
          this.onSelfbyteSentUpdate(result.data);
        } else {
          this.onOtherbyteReceiveUpdate(result.data);
        }
        break;
      case "reconnecting":
        this.reconnecting = true;
        this.reconnecting_text = "Trying to reconnect. Please check your internet connection";
        break;
      case "reconnected":
        this.reconnecting = false;
        this.fromDisconnected = true;
        this.participantsCounter = 0;
        break;
    }
  }
  onLeftUser(name) {
    this.leftName = name;
    if (this.leftName) {
      if (this.leftTimer !== null) {
        clearTimeout(this.leftTimer);
        this.leftTimer = null;
      }
      this.leftTimer = setTimeout(() => {
        this.leftName = "";
        this.leftTimer = null;
      }, 3000)
    }
  }
  onJoinUser(name) {
    this.joinName = name;
    // this.participantMap.set(data.user.userId, data.user)
    if (this.joinTimer !== null) {
      clearTimeout(this.joinTimer);
      this.joinTimer = null;
    }
    this.joinTimer = setTimeout(() => {
      this.joinName = "";
      this.joinTimer = null;
    }, 3000);
  }
  getParticipantsName(userId: string) {
    var participant = this.participantsService.getParticipantMap().get(userId)
    if (participant) {
      return participant.name;
    }
    return "";
  }
  onSelfbyteSentUpdate(data) {
    if (data.track === "p_vOu") {
      var video = data.bytesSent;
      if (this.byteSent.pVideo && this.byteSent.pVideo > 0) {
        video = video - this.byteSent.pVideo;
      }
      this.byteSent.videoByte = (video / 125000).toFixed(2);
      this.byteSent.pVideo = data.bytesSent;;
    } else if (data.track === "p_aOu") {
      var audio = data.bytesSent;
      if (this.byteSent.pAudio && this.byteSent.pAudio > 0) {
        audio = audio - this.byteSent.pAudio;
      }
      this.byteSent.audioByte = (audio / 1000).toFixed(2);
      this.byteSent.pAudio = data.bytesSent;
    }
  }
  onOtherbyteReceiveUpdate(data) {
    if (data.track === "s_vIn") {
      this.updateByteReceive('videoByte', 'pVideo', data.user, data.bytesSent, 125000);
    } else if (data.track === "s_aIn") {
      this.updateByteReceive('audioByte', 'pAudio', data.user, data.bytesSent, 1000);
    }
  }
  updateByteReceive(key, previousvalue, userId, byteReceive, divider) {
    var userData = this.userStreamMap.get(userId + "user");
    if (userData && userData.role === "presenter") {
      var data = this.getObjectFromArray(userId, this.presenterList);//this.presenterList.get(userId);
      if (data) {
        //this.presenterList.set(userId, this.getUpdateAnalyticValue(data, key, previousvalue, byteReceive, divider));
        this.updateObjectInArray(userId, this.presenterList, this.getUpdateAnalyticValue(data, key, previousvalue, byteReceive, divider));
      }
      var sharingUserData = this.getObjectFromArray(this.sharingUserByteId, this.presenterList);
      if (this.sharingUserByteId && sharingUserData) {
        this.sharingUserByte.audioByte = sharingUserData.audioByte;
        this.sharingUserByte.videoByte = sharingUserData.videoByte;
      }
      /* if (this.sharingUserByteId && this.presenterList.get(this.sharingUserByteId)) {
        this.sharingUserByte.audioByte = this.presenterList.get(this.sharingUserByteId).audioByte;
        this.sharingUserByte.videoByte = this.presenterList.get(this.sharingUserByteId).videoByte;
      } */

    } else if (userData) {
      var data = this.getObjectFromArray(userId, this.subPresenterList)//this.subPresenterList.get(userId);
      if (data) {
        //this.subPresenterList.set(userId, this.getUpdateAnalyticValue(data, key, previousvalue, byteReceive, divider));
        this.updateObjectInArray(userId, this.subPresenterList, this.getUpdateAnalyticValue(data, key, previousvalue, byteReceive, divider));
      }
    } else if (!userData && userId.includes("_screen")) {
      userData = this.userStreamMap.get(userId + "screen");
      this.sharingVideoByte = this.getUpdateAnalyticValue(this.sharingVideoByte, key, previousvalue, byteReceive, divider);
    }
  }
  getUpdateAnalyticValue(data, key, previousvalue, byteReceive, divider) {
    if (data[previousvalue] === 0) {
      data[key] = (byteReceive / divider).toFixed(2);
    } else {
      data[key] = ((byteReceive - data[previousvalue]) / divider).toFixed(2);
    }
    data[previousvalue] = byteReceive;
    return data;
  }

  getObjectFromArray(userId, myArray) {
    for (var i = 0; i < myArray.length; i++) {
      if (myArray[i].userId === userId) {
        return myArray[i];
      }
    }
    return null;
  }
  updateObjectInArray(userId, myArray, newData) {
    /* for (var i = 0; i < myArray.length; i++) {
      if (myArray[i].userId === userId) {
        return myArray[i];
      }
    } */
    var index = myArray.findIndex(x => x.userId === userId);
    myArray[index] = newData;
    return myArray;
  }


  /* setAudioAnalysis() {
    if (this.localMediaStateSelection.audio) {
      this.yuweeService.getYuWeeClient().activateAudioAnalysis((err, data) => {
        console.log("activateAudioAnalysis", err, data);
  
      });
    }
  } */
  onRemoteStreamStopped(data) {
    if (data.streamType === "screen") {

      /**
       * if(this.isSpotlightEnabled){
      this.shareSelfView.nativeElement.setAttribute("style", "display:none");
      this.gridVideo.nativeElement.setAttribute("style", "display:none;");
      this.spotlight.nativeElement.setAttribute("style", "display:block");
    }else{
      this.gridVideo.nativeElement.setAttribute("style", "display:block;");
      this.shareSelfView.nativeElement.setAttribute("style", "display:none");
    }
       */
      if (this.isSpotlightEnabled) {
        this.gridVideo.nativeElement.setAttribute("style", "display:none;");
        this.spotlight.nativeElement.setAttribute("style", "display:block");
        this.shareVideoDiv.nativeElement.setAttribute('style', 'display: none;');
        this.localvideo.nativeElement.parentElement.setAttribute('style', 'display:block !important');
        this.sharedUservideo.nativeElement.parentElement.setAttribute('style', 'display:none !important');

      } else {
        this.gridVideo.nativeElement.setAttribute("style", "display:block;");
        this.shareVideoDiv.nativeElement.setAttribute('style', 'display: none;');
        this.localvideo.nativeElement.parentElement.setAttribute('style', 'display:block !important');
        this.sharedUservideo.nativeElement.parentElement.setAttribute('style', 'display:none !important');
      }

      if (data.userId.includes("_screen")) {
        var newUserId = data.userId.replace("_screen", "screen");
        this.userStreamMap.delete(newUserId);
      } else {
        this.userStreamMap.delete(data.userId + data.streamType);
      }
      this.detachMedia("shareVideo");

      this.sharingUserByteId = null;
      this.localMediaStateSelection.shareScreen = false;
    } else if (data.streamType === "user") {

      var stream = this.userStreamMap.get(data.userId + data.streamType);
      if (!stream) {
        return;
      }
      var streamId = stream.id;
      var videoId = "videoid_" + data.userId;
      //this.presenterList.delete(data.userId);
      //this.subPresenterList.delete(data.userId);
      this.presenterList = this.presenterList.filter(function (obj) {
        return obj.id !== videoId
      });
      this.subPresenterList = this.subPresenterList.filter(function (obj) {
        return obj.id !== videoId
      });

      this.detachMedia(videoId);
      this.tileUpdate();
      this.cdr.detectChanges();
      this.userStreamMap.delete(data.userId + data.streamType);
      console.log("Stop stream done");
    }

  }

  attachToLocalVideo(stream) {
    var shareDivStyle = this.shareVideoDiv.nativeElement.getAttribute('style');
    if (shareDivStyle && shareDivStyle.includes('block')) {
      this.localvideo.nativeElement.parentElement.setAttribute('style', 'display:none !important');
    } else {
      this.localvideo.nativeElement.parentElement.setAttribute('style', 'display:block !important');
    }

    this.localvideo.nativeElement.muted = true;
    this.yuweeService.getYuWeeClient().attachMediaStream(this.localvideo.nativeElement, stream);
  }
  detachMedia(id) {
    var video = <HTMLVideoElement>document.querySelector("#" + id);
    if (!video) {
      return;
    }
    /* try {
      if (video.srcObject) {
        video.srcObject = null;
      }
    } catch (e) {
  
    }
    try {
      if (video.mozSrcObject) {
        video.mozSrcObject = null;
      }
    } catch (e) {
  
    } */
    if (video.src) {
      video.src = null;
    }
  }

  hideLocalScreenShare() {
    if (this.isSpotlightEnabled) {
      this.shareSelfView.nativeElement.setAttribute("style", "display:none");
      this.gridVideo.nativeElement.setAttribute("style", "display:none;");
      this.spotlight.nativeElement.setAttribute("style", "display:block");
    } else {
      this.gridVideo.nativeElement.setAttribute("style", "display:block;");
      this.shareSelfView.nativeElement.setAttribute("style", "display:none");
    }
    this.localMediaStateSelection.shareScreen = false;
  }
  onShareScreenStatusChange(data) {
    //this.commonService.showElement(this.shareVideoDiv);
    if (data.statusId === 2 || data.statusId === 0) {
      this.hideLocalScreenShare();
    } else if (data.statusId === 1 && this.CALL_TYPE === "onetoone") {
      this.gridVideo.nativeElement.setAttribute("style", "display:none;");
      this.spotlight.nativeElement.setAttribute("style", "display:none");
      this.shareSelfView.nativeElement.setAttribute("style", "display:block");
      console.log("**********==========*****==========================I am sharing the screen");
    }
    console.log("screenShareStatusChanged", data.data);
    console.log("screenShareStatusChanged", data.statusId);
    this.localMediaStateSelection.shareScreen = true;
  }
  onChatOpen() {
    if (this.chatOpen) {
      this.chatOpen = false;
    } else {
      this.newMessage = false;
      this.chatOpen = true;
    }

  }
  openPopUp() {
    this.popup.nativeElement.style.left = this.ivMore.nativeElement.offsetLeft + "px";
    if (!this.popup.nativeElement.style.display || this.popup.nativeElement.style.display === "none") {
      this.popup.nativeElement.style.display = "block";
    } else {
      this.popup.nativeElement.style.display = "none";
    }

  }
  makeTile() {
    //this.commonService.addElemetent(this.gridVideo.nativeElement);
    //this.tileUpdate();
    this.isSpotlightEnabled = false;
    this.isTileEnabled = true;
    this.gridVideo.nativeElement.setAttribute("style", "display:block;");
    this.spotlight.nativeElement.setAttribute("style", "display:none");
  }
  makeSpotlight() {
    this.isSpotlightEnabled = true;
    this.isTileEnabled = false;
    this.gridVideo.nativeElement.setAttribute("style", "display:none;");
    this.spotlight.nativeElement.setAttribute("style", "display:block");
    this.setInitiaSpotlight();
  }

  setInitiaSpotlight() {
    var key = [];
    key = Array.from(this.userStreamMap.keys());
    if (key && key.length > 0 && this.userStreamMap && this.userStreamMap.size > 0) {
      var streamData;
      for (var i = 0; i < key.length; i++) {
        var tempStreamData = this.userStreamMap.get(key[i]);
        if (tempStreamData.role === "presenter" && key[i] !== this.localUser.user._id + "user" && key[i] !== "local") {
          console.log("setInitiaSpotlight", key[i], this.localUser.user._id);
          streamData = tempStreamData;
          break;
        }

      }
      if (streamData && streamData.stream) {
        var spotlightVideo = document.querySelector("#spotlightVideo");
        this.yuweeService.getYuWeeClient().attachMediaStream(spotlightVideo, streamData.stream);
        this.speakerName = this.getParticipantsName(streamData.userId);
      }

    }

  }

  onAudioSelect() {
    if (this.localMediaStateSelection.audio) {
      this.localMediaStateSelection.audio = false;
      this.localAudioVideoIconSelection.audio = "mic_off-white-18dp.svg";
    } else {
      this.localMediaStateSelection.audio = true;
      this.localAudioVideoIconSelection.audio = "mic-white-18dp.svg";
    }

  }
  onVideoSelect() {
    if (this.localMediaStateSelection.video) {
      this.localMediaStateSelection.video = false;
      this.localAudioVideoIconSelection.video = "videocam_off-white-18dp.svg";
    } else {
      this.localMediaStateSelection.video = true;
      this.localAudioVideoIconSelection.video = "videocam-white-18dp.svg";
    }
  }

  joinAfterSelections() {
    this.yuweeService.getYuWeeClient().getMediaStream({ "audio": this.localMediaStateSelection.audio, "video": this.localMediaStateSelection.video });
  }
  openWhiteboard() {
      this.showSnackBar("Comming soon!!");
  }
  showSnackBar(message: string){
    this.snackBar.open(message, '', {
      duration: 1000,
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
    });
  }
  changeResolution(resolutionName) {
    this.yuweeService.getYuWeeClient().updateMyResolution(resolutionName)
    this.updatingResolution = resolutionName;
  }
}


@Component({
  selector: 'dialog-join-meeting',
  templateUrl: 'dialog-join-meeting.html',
  styleUrls: ['./meeting.component.css']
})
export class DialogJoinMeeting {
  isLogin = true;
  joiningForm: FormGroup;
  emailRegx = /^(([^<>+()\[\]\\.,;:\s@"-#$%&=]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,3}))$/;
  constructor(private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<DialogJoinMeeting>,
    @Inject(MAT_DIALOG_DATA) public data: MeetingJoinModel, private router: Router) {
    if (!data.email) {
      this.isLogin = false;
    }
    this.joiningForm = this.formBuilder.group({
      email: [data.email ? data.email : "subp" + new Date().getTime() + "@subp.com", [Validators.required, Validators.pattern(this.emailRegx)]],
      name: [data.name ? data.name : null, [Validators.required]],
      role: ["subPresenter", [Validators.required]],
      meetingId: [data.meetingId ? data.meetingId : null, [Validators.required]]
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
    this.router.navigate(['dashboard']);
  }
  submit() {
    //console.log("Form data ", this.joiningForm.value);
    if (!this.joiningForm.valid) {
      return;
    }
    this.data.name = this.joiningForm.get("name").value;
    this.data.email = this.joiningForm.get("email").value;
    this.data.meetingId = this.joiningForm.get("meetingId").value;
    this.data.role = this.joiningForm.get("role").value;
    this.dialogRef.close(this.data);
  }
}
