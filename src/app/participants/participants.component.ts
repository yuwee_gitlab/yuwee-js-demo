import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { JoinUserModel } from '../model/join.user.model';
import { ParticipantsService } from '../services/participants.service';
import { YuWeeService } from '../services/yuwee.service';
interface Test {
  name: string;
  add: string;
}
@Component({
  selector: 'app-participants',
  templateUrl: './participants.component.html',
  styleUrls: ['./participants.component.css'],
})

export class ParticipantsComponent implements OnInit, OnDestroy {

  //participantMap: Array<JoinUserModel>;
  participantsMap = new Map<string, JoinUserModel>();
  //participantKeys = [];
  localUser: any;
  myMap = new Map<string, Test>();
  myKeys: []//IterableIterator<string>;
  /* participantArray = [
   'Pepper dsdfsdfds fdsf  sdf sljslkfj lksdj ',
   'Salt lksdj fklsdj fklj dsfklj sdfklj asdklfj ',
   'Paprika',
   'Apple',
   'Termaric',
   'Banana',
   'Lemon',
   'Pepper',
   'Salt',
   'Paprika',
   'Apple',
   'Termaric',
   'Banana',
   'Lemon',
   'Pepper',
   'Salt',
   'Paprika',
   'Apple',
   'Termaric',
   'Banana',
   'Lemon',
   'Pepper',
   'Salt',
   'Paprika',
   'Apple',
   'Termaric',
   'Banana',
   'Lemon'
   
 ]; */
  onGeneralEventEmitter: Subscription;
  onMeetingRegistered: Subscription;
  //registerData: any;
  constructor(private participantsService: ParticipantsService, private yuweeService: YuWeeService, private cdr: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.onGeneralEventEmitter = this.yuweeService.onGeneralEventEmitter.subscribe((data) => {
      this.updateData(data);
    });
  }
  getUser() {
    try {
      var data = localStorage.getItem('yuvitimeStorage');
      var temp = JSON.parse(data).user
      return temp;
    } catch (err) {
      return {};
    }
  }
  getAudioURL(obj): string {
    var audioIconURL;
    if (obj.isAudioOn === true || obj.isAudioOn === "true") {
      audioIconURL = "mic-white-18dp.svg"
    } else {
      audioIconURL = "mic_off-white-18dp.svg";
    }
    return audioIconURL;
  }
  getHandURL(obj) {
    var handURL = "";
    if (obj.handRaise) {
      handURL = "hand-raise.svg"
    }
    return handURL;
  }

  getVideoURL(obj): string {
    var videoIconURL: string;
    if (obj.isVideoOn == true || obj.isVideoOn === "true") {
      videoIconURL = "videocam-white-18dp.svg";
    } else {
      videoIconURL = "videocam_off-white-18dp.svg";
    }
    return videoIconURL;
  }
  updateData(data) {
    //console.log("User join or left ", data);
    if (data.type === "registered") {
      this.getActiveParticipants();
      var localStorageData = localStorage.getItem("meetingSession");
      if (localStorageData) {
        var retisterData = JSON.parse(localStorageData);
        this.localUser = {
          isCallAdmin: retisterData.isCallAdmin,
          isDiscussion: retisterData.isDiscussion,
          isPresenter: retisterData.isPresenter,
          role: retisterData.role,
          user: retisterData.sessionTokenInfo ? retisterData.sessionTokenInfo.user : {}
        }
        if (!this.localUser.user || !this.localUser.user._id) {
          this.localUser.user = this.getUser();
        }
      }
    }
    else if (data.type === "join") {
      var user = data.user;
      if (user && user.info) {
        user.audioIconURL = this.getAudioURL(user.info);
        user.videoIconURL = this.getVideoURL(user.info);
      }

      user.status = this.getStatus(user)

      var joinUserModel: JoinUserModel;
      var name = user.info.name;
      if (this.localUser && this.localUser.user && this.localUser.user._id === user._id) {
        name = name + "(me)"
      }

      joinUserModel = {
        _id: user.info._id,
        name: name,
        isCallAdmin: user.info.isCallAdmin,
        isPresenter: user.info.isPresenter,
        isSubPresenter: user.info.isSubPresenter,
        isVideoOn: user.info.isVideoOn,
        isAudioOn: user.info.isAudioOn,
        image: user.info.image,
        email: user.info.email,
        audioIconURL: user.audioIconURL,
        videoIconURL: user.videoIconURL,
        status: user.status
      }
      this.participantsMap.set(joinUserModel._id, joinUserModel)
    } else if (data.type === "left") {
      setTimeout(() => {
        this.participantsMap.delete(data.user.userId)
      }, 500);

      //this.participantsMap = this.participantArray.filter((item) => item.userId !== data.user.userId);
    } else if (data.type === "change") {
      var userChagne: any = this.participantsMap.get(data.user.userId);
      if (userChagne) {
        userChagne.isAudioOn = data.user.info.isAudioOn
        userChagne.isVideoOn = data.user.info.isVideoOn
        userChagne.audioIconURL = this.getAudioURL(data.user.info);
        userChagne.videoIconURL = this.getVideoURL(data.user.info);
        this.participantsMap.set(data.user.userId, userChagne);
      }
    } else if (data.type === "muted" || data.type === "selfMuted") {
      var userChagne: any = this.participantsMap.get(data.user.userId);
      if (userChagne) {
        userChagne.isAudioOn = !data.user.isMuted
        userChagne.audioIconURL = this.getAudioURL(userChagne);
        this.participantsMap.set(data.user.userId, userChagne);
      }
    } else if (data.type === "handRaise") {
      var userChagne: any = this.participantsMap.get(data.user.userId);
      if (userChagne) {
        userChagne.handRaise = true;
        userChagne.handURL = this.getHandURL(userChagne);
        this.participantsMap.set(data.user.userId, userChagne);
      }
    } else if (data.type === "handLower") {
      var userChagne: any = this.participantsMap.get(data.user.userId);
      if (userChagne) {
        userChagne.handRaise = false;
        userChagne.handURL = this.getHandURL(userChagne);
        this.participantsMap.set(data.user.userId, userChagne);
      }
    } else if (data.type === "myRoleUpdate") {
      var userChagne: any = this.participantsMap.get(data.user.userId);
      if (userChagne) {
        userChagne.status = this.getStatus(data.user);
        this.participantsMap.set(data.user.userId, userChagne);
      }
    }
    else if (data.type === "roleUpdate") {
      var userChagne: any = this.participantsMap.get(data.user.userId);
      if (userChagne) {
        userChagne.status = this.getStatus(data.user);
        this.participantsMap.set(data.user.userId, userChagne);
      }
    }
    //this.participantKeys = Array.from(this.participantsMap.keys());
    //console.log("sdfdsfdsfdsf", this.participantKeys);
    this.cdr.detectChanges();
    this.participantsService.setParticipantMap(this.participantsMap);
  }

  getActiveParticipants() {
    this.yuweeService.getYuWeeClient().fetchActiveParticipantsList((err, data) => {
      console.log("Active participantArray", data);
      if (data && data.result && data.result.length > 0) {
        for (var i = 0; i < data.result.length; i++) {
          var user = data.result[i];
          var name = user.name;
          if (this.localUser.user._id === user._id) {
            name = name + "(me)"
          }
          this.participantsMap.set(user._id, {
            _id: user._id,
            name: name,
            isCallAdmin: user.isCallAdmin,
            isPresenter: user.isPresenter,
            isSubPresenter: user.isSubPresenter,
            isVideoOn: user.isVideoOn,
            isAudioOn: user.isAudioOn,
            image: user.image,
            email: user.email ? user.email : "",
            audioIconURL: this.getAudioURL(user),
            videoIconURL: this.getVideoURL(user),
            status: this.getStatus(user)
          });
        }
      }
      //this.participantKeys = Array.from(this.participantsMap.keys());

    });
    this.participantsService.setParticipantMap(this.participantsMap);
  }
  makeAdmin(participant) {

  }
  makePresenter(participant) {
    this.updateRole(participant._id, participant.status, "presenter", 'isPresenter');
  }
  makeSubPresenter(participant) {
    this.updateRole(participant._id, participant.status, "subPresenter", 'isSubPresenter');
  }
  makeViewer(participant) {
    this.updateRole(participant._id, participant.status, "viewer", 'viewer');
  }
  removeParticipant(participant) {
    this.yuweeService.getYuWeeClient().dropParticipant(participant._id, (err, data) => {
      console.log("dropParticipant", err, data);

    });
  }
  muteParticipant(participant) {
    if (!this.localUser.isCallAdmin || !this.localUser.isPresenter) {
      return;
    }
    if (this.localUser && this.localUser.user && this.localUser.user._id === participant._id) {
      this.yuweeService.onGeneralEventEmitter.next({
        user: {
          userId: this.localUser.user._id,
        },
        type: 'toggleAudio'
      });
    } else {
      this.yuweeService.getYuWeeClient().toggleParticipantAudio(participant._id, participant.isAudioOn ? false : true, (err, data) => {
        console.log("toggleParticipantAudio", err, data);
        var user = this.participantsMap.get(participant._id);
        if (!err) {
          user.isAudioOn = participant.isAudioOn ? false : true;
          user.audioIconURL = this.getAudioURL(user);
          this.participantsMap.set(participant._id, user);
        }
      });
    }
    this.participantsService.setParticipantMap(this.participantsMap);
  }
  stopParticipantVideo(participant) {
    if (!this.localUser.isCallAdmin || !this.localUser.isPresenter) {
      return;
    }
    if (this.localUser && this.localUser.user && this.localUser.user._id === participant._id) {

    } else {

    }
  }
  updateRole(userId, oldRole, newRole, key) {
    var oldRoleTemp = oldRole.replace("-", "");
    if (newRole.toLowerCase() === oldRoleTemp.toLowerCase()) {
      return;
    }
    this.yuweeService.getYuWeeClient().updateParticipantsRole(userId, newRole, (err, data) => {
      console.log("updateParticipantsRole", data);
      var user = this.participantsMap.get(userId);
      user.isCallAdmin = false;
      user.isPresenter = false;
      user.isSubPresenter = false;
      user[key] = true;
      user.status = this.getStatus(user)
      this.participantsMap.set(userId, user);

    });
    this.participantsService.setParticipantMap(this.participantsMap);
  }
  getStatus(user) {
    var status: string;
    if(user.viewer){
      status = "Viewer";
    }else if (user.role === "viewer" || user.newRole === "viewer") {
      status = "Viewer";
    } else if (user.isSubPresenter || user.role === "subPresenter" || user.newRole === "subPresenter") {
      status = "Sub-Presenter";
    } else if (user.isCallAdmin && user.isPresenter || user.role === "admin" || user.newRole === "admin") {
      status = "Admin";
    } else if (user.isPresenter || user.role === "presenter" || user.newRole === "presenter") {
      status = "Presenter";
    } else if (user.isCallAdmin || user.role === "admin" || user.newRole === "admin") {
      status = "Admin";
    } else {
      status = "Viewer";
    }
    return status;
  }
  ngOnDestroy(): void {
    if (this.onMeetingRegistered) {
      this.onMeetingRegistered.unsubscribe();
    }
    if (this.onGeneralEventEmitter) {
      this.onGeneralEventEmitter.unsubscribe();
    }
  }
}
