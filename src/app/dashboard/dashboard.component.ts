import { Component, ElementRef, Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';
import { CommonService } from "../services/common.service";
import { Config } from "../util/config";
import { YuWeeService } from "../services/yuwee.service";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, OnDestroy {
  @ViewChild('linkAddresses') linkAddresses: ElementRef<HTMLAnchorElement>
  meetingDetails: any;
  timeout = null;
  callData: any;
  //onOneToOneCallListner: Subscription;
  horizontalPosition: MatSnackBarHorizontalPosition = 'right';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  onOneToOneCallListner: Subscription;
  getUserInfoListener: Subscription;
  isCalling: boolean;
  constructor( public dialog: MatDialog, private snackBar: MatSnackBar, private router: Router, private commonService: CommonService, private yuweeService: YuWeeService) {
    this.newMeetings = this.newMeetings.bind(this);
    this.yuweeService.getYuWeeClient();
  }

  ngOnInit(): void {
    this.isCalling = false;
    if (!this.commonService.isLoggedIn()) {
      this.router.navigate(['sdklogin']);
    } else {
      var data = localStorage.getItem("userData");
      if (data) {
        var userData = JSON.parse(data)
        this.yuweeService.createUserSession(userData.email, userData.password);
      }

    }

    this.onOneToOneCallListner = this.yuweeService.onOneToOneCallListner.subscribe(data => {
      if (data && data.data.messageType === "CALL") {
        var storageData = localStorage.getItem("yuvitimeStorage");
        var userId;
        if (storageData) {
          var jsonData = JSON.parse(storageData);
          userId = jsonData.user._id
        }
        if (userId && userId === data.data.destination.destinationId) {
          this.isCalling = true;
          this.callData = data.data;
        }
      }
    });
    this.getUserInfoListener = this.yuweeService.getUserInfoListener.subscribe(() => {
      this.afterSessionCreated();
    });
    /*  this.onOneToOneCallListner = this.yuweeService.onOneToOneCallListner.subscribe(()=>{
 
     });
  */
  }

  openChatRoomDialog() {
    /* const dialogRef = this.dialog.open(DialogChatRoom, {
      width: '400px',
      height: '350px',
      panelClass: "my-custom-dialog-class",
      data: "",
    });

    dialogRef.afterClosed().subscribe(result => {
      if (!result) {

      } else {
        this.createChatRoom(result);
      }
    });
  }
  createChatRoom(createData) {
    this.yuweeService.getYuWeeClient().fetchChatRoombyEmails([createData.email], false, createData.groupName, (err, data) => {
      console.log("fetchChatRoom", err, data);
    }); */
  }


  afterSessionCreated() {
    this.yuweeService.getYuWeeClient().fetchContactList(data => {

    });
  }
  openDocumentation() {
    this.router.navigate(['documentation']);
  }
  goToChatRoom() {
    this.router.navigate(['chat']);
  }
  acceptCall() {
    //this.isCalling = false;

    this.router.navigate(['joinMeeting', { type: "onetoone", callId: this.callData.callId, mediaType: this.callData.callType }], { state: { callId: this.callData.callId, callData: this.callData } });

  }

  rejectCall() {
    this.isCalling = false;
    this.yuweeService.getYuWeeClient().rejectIncomingCall();
  }
  ngOnDestroy(): void {
    if (this.getUserInfoListener) {
      this.getUserInfoListener.unsubscribe();
    }
    if (this.onOneToOneCallListner) {
      this.onOneToOneCallListner.unsubscribe();
    }
  }
  logout() {
    localStorage.removeItem("meetingDetail");
    localStorage.removeItem("yuvitimeStorage");
    localStorage.removeItem("userData");
    localStorage.removeItem("meetingSession");
    localStorage.removeItem("progressMeeting");
    this.router.navigate(['sdklogin']);

  }
  joinMeetings() {
    this.router.navigate(["joinMeeting"]);
  }

  p2pCall(type) {

    this.router.navigate(["joinMeeting", { type: "onetoone" }]);
  }
  newMeetings(result) {
    var userData = localStorage.getItem("yuvitimeStorage");
    var data;
    if (userData) {
      data = JSON.parse(userData);
      console.log("Loggin user data", data);
    }
    if (!data) {
      console.log("Please login to host a meeting");
      return;
    }
    var meetingParams = {
      "meetingName": result.name,
      "maxAllowedParticipant": 100,
      "callMode": 1,
      "meetingStartTime": new Date().getTime() + 10000,
      "meetingExpireDuration": 102000,
      "presenters": [data.user.email],
      "callAdmins": [data.user.email],
      "activateVAD": true
    };
    let that = this

    this.yuweeService.getYuWeeClient().hostMeeting(meetingParams, function (err, result) {
      if (err) {
        console.log(err)
      } else {
        console.log(result);
        // var data = response.result;
        if (result) {
          localStorage.setItem("meetingDetail", JSON.stringify(result));
          that.meetingDetails = result;
          /* this.meetingDetails = "Meeting id : "+data.meetingTokenId+"\n"+
          "Meeting name : "+data.meetingName+"\n"+
          "Admins : " + data.callAdmins +"\n"+
          "Passcode : " +data.passcode +"\n"+
          "Presenters : "+ data.presenters +"\n"+
          "Presenters' Passcode : "+data.presenterPasscode +"\n"+
          "Sub-Presenter's Passcode : "+ data.subPresenterPasscode +"\n"+
          "Meeting Expiration Time : "+ (data.meetingStartTime + data.meetingExpireDuration) +"\n"+
          "Invitation Url : "+Config.appUrl+"joinMeeting?meetingId="+data.meetingTokenId;; */
          that.meetingDetails.presenterPasscode = encodeURIComponent(that.commonService.encrypt(result.presenterPasscode));
          that.meetingDetails.subPresenterPasscode = encodeURIComponent(that.commonService.encrypt(result.subPresenterPasscode));;
          that.meetingDetails.passcode = encodeURIComponent(that.commonService.encrypt(result.passcode));
          that.meetingDetails.mainURL = that.getMeetingURL(result.meetingTokenId, result.presenterPasscode, result.subPresenterPasscode, result.passcode);
          //that.meetingDetails.presenterURL = that.getMeetingURL(result.meetingTokenId, result.presenterPasscode); 
          //that.meetingDetails.subPresenterURL = that.getMeetingURL(result.meetingTokenId, result.subPresenterPasscode); 
          //that.meetingDetails.viewURL = that.getMeetingURL(result.meetingTokenId, result.passcode);
          that.meetingDetails.expire = new Date(result.meetingStartTime + result.meetingExpireDuration).toLocaleString();
        }
      }

    });
  }

  getMeetingURL(meegingId: string, presenterPasscode: string, subPresenterPasscode: string, viewPasscode: string): string {
    var URL = Config.appUrl + "joinMeeting?meetingId=" + meegingId;
    var code = presenterPasscode + "apvpa" + subPresenterPasscode + "apvpa" + viewPasscode;
    //var encrypted = this.commonService.encrypt(code);
    var base64Code = btoa(code);
    console.log("code", URL + "&pq=" + base64Code);

    return URL + "&pq=" + encodeURIComponent(base64Code);

  }
  onCopyToClipboard() {
    if (this.timeout) {
      clearTimeout(this.timeout);
      this.timeout = null;
    }
    this.linkAddresses.nativeElement.setAttribute("style", "background-color: #a49f995e;");
    this.timeout = setTimeout(() => {
      this.linkAddresses.nativeElement.setAttribute("style", "background-color: transparent;");
      this.timeout = null;
    }, 3000);
    this.openSnackBar();
  }
  openSnackBar() {
    this.snackBar.open('Link copied !!', '', {
      duration: 1000,
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
    });
  }

  openDialog(type): void {
    const dialogRef = this.dialog.open(DialogNewMeeting, {
      width: '400px',
      data: type,
    });

    dialogRef.afterClosed().subscribe(result => {
      if (!result) {

      } else {
        this.newMeetings(result)
      }


    });
  }
}


@Component({
  selector: 'dialog-new-meeting',
  templateUrl: 'dialog-new-meeting.html',
  styleUrls: ['./dashboard.component.css']
})
export class DialogNewMeeting {
  meetingForm: FormGroup;
  @ViewChild('meeginName') meeginName: ElementRef<HTMLInputElement>;
  emailRegx = /^(([^<>+()\[\]\\.,;:\s@"-#$%&=]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,3}))$/;
  constructor(private formBuilder: FormBuilder, public dialogRef: MatDialogRef<string>, @Inject(MAT_DIALOG_DATA) public data: string) {
    this.meetingForm = this.formBuilder.group({
      name: [null, [Validators.required, Validators.minLength(3)]],
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  submit() {
    if (!this.meetingForm.get("name").value) {
      return;
    }
    /* console.log("Form data ", this.meetingForm.value);
    if (!this.meetingForm.valid) {
      return;
    }
    var name = this.meetingForm.get("name").value; */
    console.log("Form data ",this.meetingForm.get("name").value);
    this.dialogRef.close({ name: this.meetingForm.get("name").value, type: this.data });
  }
}


/* @Component({
  selector: 'new-chat-room',
  templateUrl: 'dialog-new-chat-room.html',
  styleUrls: ['./dashboard.component.css']
})
export class DialogChatRoom {
  chatForm: FormGroup;
  constructor(private formBuilder: FormBuilder, public dialogRef: MatDialogRef<string>, @Inject(MAT_DIALOG_DATA) public data: string) {
    this.chatForm = this.formBuilder.group({
      groupName: [null, [Validators.required, Validators.minLength(3)]],
      email: [null, [Validators.required, Validators.pattern(Config.emailRegx)]]
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  submit() {
    if (!this.chatForm.valid) {
      return;
    }

   
    console.log("Form data ", this.chatForm.get("email").value);
    this.dialogRef.close({ email: this.chatForm.get("email").value, groupName: this.chatForm.get("groupName").value });
  }
}
 */
