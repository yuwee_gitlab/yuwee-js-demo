import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Config } from '../util/config';
import { YuWeeService } from '../services/yuwee.service';
import { Subscription } from 'rxjs';
import { CommonService } from '../services/common.service';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';

@Component({
  selector: 'app-sdklogin',
  templateUrl: './sdklogin.component.html',
  styleUrls: ['./sdklogin.component.css']
})
export class SDKLoginComponent implements OnInit, OnDestroy {
  loginForm: FormGroup;
  getUserInfoListener: Subscription;
  horizontalPosition: MatSnackBarHorizontalPosition = 'right';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  constructor(private snackBar: MatSnackBar, private router: Router, private commonService: CommonService, private yuweeService: YuWeeService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.gotoDashboard();

    this.loginForm = this.formBuilder.group({
      email: [null, [Validators.required, Validators.pattern(Config.emailRegx)]],
      password: [null, [Validators.required, Validators.minLength(6)]]

    });

    this.getUserInfoListener = this.yuweeService.getUserInfoListener.subscribe(data => {
      if(data.access_token && data.access_token !== "null"){
        localStorage.setItem("yuvitimeStorage", JSON.stringify(data));
        this.gotoDashboard();
        this.showSnackBar('Login successful');
      }else{
        this.showSnackBar('User not found');
      }
    });
  }
  showSnackBar(message) {
    this.snackBar.open(message, '', {
      duration: 1000,
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
    });

  }
  gotoDashboard() {
    if (this.commonService.isLoggedIn()) {
      this.router.navigate(['dashboard'])
    }
  }

  gotoSignup(){
    this.router.navigate(['sdksignup']);
  }
  ngOnDestroy(): void {
    if (this.getUserInfoListener) {
      this.getUserInfoListener.unsubscribe();

    }
  }
  submit() {
    if (!this.loginForm.valid) {
      return;
    }
    var userData = {
      email: this.loginForm.get("email").value,
      password: this.loginForm.get("password").value
    }
    localStorage.setItem("userData", JSON.stringify(userData));
    this.yuweeService.createUserSession(this.loginForm.get("email").value, this.loginForm.get("password").value);
  }

}
