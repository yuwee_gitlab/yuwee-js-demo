import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SDKLoginComponent } from './sdklogin.component';

describe('SDKLoginComponent', () => {
  let component: SDKLoginComponent;
  let fixture: ComponentFixture<SDKLoginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SDKLoginComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SDKLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
