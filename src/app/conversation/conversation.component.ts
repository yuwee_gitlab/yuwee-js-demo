import { Component, ElementRef, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { DialogChatRoom } from '../dialog/chatroom/dialogchatroom.component';
import { CommonService } from '../services/common.service';
import { YuWeeService } from '../services/yuwee.service';

@Component({
  selector: 'app-conversation',
  templateUrl: './conversation.component.html',
  styleUrls: ['./conversation.component.css']
})
export class ConversationComponent implements OnInit, OnDestroy {
  conversationList: any;
  getUserInfoListener: Subscription;
  onGeneralEventEmitter: Subscription;
  selectedRoomId: string;
  localUser: any;
  isLoading = false;
  searchModel: string;
  currentObject: any;

  @ViewChild('messageDiv') private messageDiv: ElementRef;
  constructor(private commonService: CommonService, public dialog: MatDialog, private yuweeService: YuWeeService) {

  }

  ngOnInit(): void {
    this.localUser = {
      user: {

      }
    }
    this.isLoading = true;
    this.conversationList = [];
    var data = localStorage.getItem("userData");
    if (data) {
      var userData = JSON.parse(data)
      this.yuweeService.createUserSession(userData.email, userData.password);
    }
    this.getUserInfoListener = this.yuweeService.getUserInfoListener.subscribe(() => {
      this.afterSessionCreated();
    });
    this.onGeneralEventEmitter = this.yuweeService.onGeneralEventEmitter.subscribe(data => {
      switch (data.type) {
        case "chatList":
          this.conversationList = data.data.results;
          this.isLoading = false;
          break;
        case "markReadSuccess":
          this.updateMarkRead(data.data.result);
          break;
        case "markReadFail":
          console.log("markReadFail", data);
          break;
        case "message_receive":
          this.updateConversationList(data);
          break;
        case "message_send":
          this.updateConversationList(data);
          break;
        case "deleteRoomSuccess":
          this.deleteConversationFromList(data.data.roomId);
          break;
        case "deleteRoomFailed":
          break;
        case "message_send":

          break;
      }

    });
  }

  @Output() public select: EventEmitter<{}> = new EventEmitter();

  public onSelect(obj: any): void {
    this.currentObject = obj;
    this.select.emit(obj);
  }


  openChatRoomDialog() {
    const dialogRef = this.dialog.open(DialogChatRoom, {
      width: '400px',
      height: '350px',
      panelClass: "my-custom-dialog-class",
      data: "",
    });

    dialogRef.afterClosed().subscribe(result => {
      if (!result) {

      } else {
        this.createChatRoom(result);
      }
    });
  }
  createChatRoom(createData) {
    this.yuweeService.getYuWeeClient().fetchChatRoombyEmails([createData.email], false, createData.groupName, (err, data) => {
      console.log("fetchChatRoom", err, data);
    });
  }
  updateConversationList(data) {
    var tempMessage: any;
    var destination = data.message.destination;
    if (data.message.isGroup) {
      destination = this.getDestination(data.message.receivers);
    }

    tempMessage = {
      contactUserId: data.message.sender.senderId,
      email: destination ? destination.email : "",
      image: "",
      isBlocked: null,
      isDeleted: null,
      isGroupChat: data.message.isGroup,
      lastMessage: data.message.message,
      lastMessageByName: data.message.receivers,
      lastMessageTime: data.message.dateOfCreation,
      lastMessageisEncrypted: data.message.isEncrypted,
      lastModificationTime: data.message.dateOfCreation,
      name: destination ? destination.name : "",
      roomId: data.message.roomId,
      status: 1,
      unreadMessageCount: 0,
    }
    if (data.message.isGroup) {
      tempMessage.groupInfo = data.message.group ? data.message.group : {};
    }
    this.updateLastMessage(data.message.roomId, tempMessage);
  }
  deleteConversationFromList(roomId) {
    var index;
    for (var i in this.conversationList) {
      if (this.conversationList[i].roomId === roomId) {
        index = i;
        break;
      }
    }
    this.conversationList.splice(index, 1);
  }
  getDestination(membersInfo) {
    var friendObject = membersInfo.find((f) => {

      if ((f._id).toString() != this.localUser.user._id) {
        return f
      }
    });
    return friendObject;
  }
  removeGroup(conversation) {
    this.yuweeService.getYuWeeClient().deleteChatRoom(conversation.roomId);
  }
  checkGroupAdmin(conversation) {
    if (conversation.isGroupChat && conversation.groupAdmins && conversation.groupAdmins.includes(this.localUser.user._id)) {
      return true;
    } else {
      return false;
    }
  }

  onMenuOpen(event) {
    event.stopPropagation();
  }
  updateLastMessage(roomId, conversation) {
    var index;
    for (var i in this.conversationList) {
      if (this.conversationList[i].roomId === roomId) {
        index = i;
        break;
      }
    }
    this.conversationList.splice(index, 1);
    this.conversationList.splice(0, 0, conversation)
  }
  updateMarkRead(roomId) {
    for (var i in this.conversationList) {
      if (this.conversationList[i].roomId === roomId) {
        this.conversationList[i].unreadMessageCount = 0;
        break;
      }
    }
  }

  scrollToBottom(): void {
    try {
      this.messageDiv.nativeElement.scrollTop = this.messageDiv.nativeElement.scrollHeight;
    } catch (err) { }
  }

  getName(conversation) {
    return this.commonService.getName(conversation);
  }
  getDateTime(conversation: any) {
    if (conversation.lastMessageTime) {
      return new Date(conversation.lastMessageTime).toLocaleString();
    } else if (conversation.dateOfModification) {
      return new Date(conversation.dateOfModification).toLocaleString();
    } else if (conversation.lastModificationTime) {
      return new Date(conversation.lastModificationTime).toLocaleString();
    }
    else {
      return new Date(conversation.dateOfCreation).toLocaleString();
    }

  }
  getOpacity(conversation) {
    if (conversation.unreadMessageCount === undefined || conversation.unreadMessageCount !== 0) {
      return 1;
    } else {
      return 0.7;
    }
  }

  getAllChats(roomId) {
    this.selectedRoomId = roomId;
    this.yuweeService.onGeneralEventEmitter.next({ type: "getAllChats", roomId: roomId });
    this.markAsRead(roomId);
  }
  markAsRead(roomId) {
    this.yuweeService.getYuWeeClient().markMessagesReadInRoom(roomId)
  }
  ngOnDestroy(): void {
    if (this.getUserInfoListener) {
      this.getUserInfoListener.unsubscribe();
    }
    if (this.onGeneralEventEmitter) {
      this.onGeneralEventEmitter.unsubscribe();
    }
  }

  afterSessionCreated() {
    this.localUser.user = this.getUser();
    this.yuweeService.getYuWeeClient().fetchChatList((data) => {


    });
  }

  getUser() {
    try {
      var data = localStorage.getItem('yuvitimeStorage');
      var temp = JSON.parse(data).user
      return temp;
    } catch (err) {
      return {};
    }
  }
}
