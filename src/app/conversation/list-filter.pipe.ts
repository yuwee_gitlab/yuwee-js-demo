import { Pipe, PipeTransform } from '@angular/core';
import { CommonService } from '../services/common.service';

@Pipe({
  name: 'listFilter'
})
export class ListFilterPipe implements PipeTransform {
  constructor(private commonService: CommonService){
    
  }

  transform(list: any[], filterText: string): any {
    return list ? list.filter(item => this.commonService.getName(item).search(new RegExp(filterText, 'i')) > -1) : [];
  }

}
