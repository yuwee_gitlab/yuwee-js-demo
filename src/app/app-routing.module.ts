import { Component, NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConversationComponent } from './conversation/conversation.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MeetingComponent } from './meeting/meeting.component';
import { ParticipantsComponent } from './participants/participants.component';
import { SDKLoginComponent } from './sdklogin/sdklogin.component';
import { SDKSignupComponent } from './sdksignup/sdksignup.component';

const routes: Routes = [
{
  path: 'sdklogin',
  component: SDKLoginComponent
},
{
  path: 'sdksignup',
  component: SDKSignupComponent
},

{
  path: '',
  component: DashboardComponent
},
{
  path: 'dashboard',
  component: DashboardComponent
},
{
  path: 'joinMeeting',
  component: MeetingComponent
},
{
  path: 'chat',
  component: ConversationComponent
},
{
  path: 'participants',
  component: ParticipantsComponent
},
{
  path: '**', redirectTo: '', pathMatch: 'full'
}

];
@NgModule({
  imports: [RouterModule.forRoot(routes/* , { useHash: true } */)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
