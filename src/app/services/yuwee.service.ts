import { Injectable } from '@angular/core';
import { Config } from '../util/config';
import { Subject } from 'rxjs';
import * as YuWee from '../lib/YuWee.min.js';
declare var YuWee: any;

@Injectable({ providedIn: 'root' })
export class YuWeeService {
    yuWeeClient: any;
    localStreamReceiver = new Subject<any>();
    onRemoteStream = new Subject<any>();
    getUserInfoListener = new Subject<any>();
    leaveMeetingListener = new Subject<any>();
    onAccountCreation = new Subject<any>();
    onGeneralEventEmitter = new Subject<any>();
    onOneToOneCallListner = new Subject<any>();
    constructor() {
        if (!this.yuWeeClient) {
            this.yuWeeClient = new YuWee();
            this.setYuWeehanders();
            this.yuWeeClient.setAppCredentials(Config.callAppId, Config.callAppSecret, Config.callClientId, Config.env, true);
        }
    }

    createUser(name, email, password) {
        this.yuWeeClient.createUser(name, email, password);
    }
    createUserSession(email: string, password: string) {
        if (email && password) {
            this.yuWeeClient.createSessionViaCredentials(email, password, 1800000);
        }

    }
    getYuWeeClient() {
        return this.yuWeeClient;
    }
    accountCreateSuccess(data) {
        console.log("user created", data);
        this.onAccountCreation.next({ status: "success", data: data });
    }
    accountCreateFailure(data) {
        console.log("user creation fail", data);
        this.onAccountCreation.next({ status: "Failure", data: data });
    }
    sessionCreateSuccess(userInfo) {
        localStorage.setItem("yuvitimeStorage", JSON.stringify(userInfo));
        this.getUserInfoListener.next(userInfo);

    }
    sessionCreateFailure() {

    }
    callSetupFailure() {

    }
    allUsersOffline() {

    }
    allUsersBusy() {

    }
    readyToInitiateCall(err, data) {
        this.onOneToOneCallListner.next({ type: "READY_TO_INITIATE", data: data });

    }
    streamRequestDowngraded() {

    }
    streamCapturefailed(error) {
        console.log("Local stream failed", error);
    }
    onLocalStreamReceived(localStream, role) {
        //Local stream received
        console.log("Local stream received", role);
        this.localStreamReceiver.next({ data: localStream, role: role });
    }
    showIncomingCallNotification(data) {
        console.log("showIncomingCallNotification", data);
        this.onOneToOneCallListner.next({ data })

    }
    invitationExpired() {

    }
    callHangUpSuccess() {
        console.log("callHangUpSuccess");
        this.leaveMeetingListener.next();
    }
    initiatorOffline() {

    }
    onRemoteStreamReceived(streamInfoObj) {
        console.log("onRemoteStreamReceived", streamInfoObj);
        if (streamInfoObj && streamInfoObj.isP2P) {
            this.onOneToOneCallListner.next({ data: streamInfoObj, type: "remoteStream" });
        } else {
            this.onRemoteStream.next({ data: streamInfoObj, type: 'remoteStream' });
        }

    }
    remoteStreamAvailable(streamInfoObj) {
        /*  this.onRemoteStream.next({
             data: streamInfoObj,
             type: 'available'
         }
         ); */
        console.log("remoteStreamAvailable", streamInfoObj);
        if (streamInfoObj && streamInfoObj.isP2P) {
            this.onOneToOneCallListner.next({ data: streamInfoObj, type: "remoteStream" });
        } else {
            this.onRemoteStream.next({ data: streamInfoObj, type: 'remoteStream' });
        }
    }
    remoteStreamStopped(dataObj) {
        console.log("remoteStreamStopped", dataObj);
        
        this.onRemoteStream.next({
            data: dataObj,
            type: 'stopped'
        }
        );
    }
    remoteScreenShared() {
        console.log("remoteScreenShared");


    }
    callRejected(data) {
        console.log("callRejected", data);
    }
    callAccepted(data) {
        this.onOneToOneCallListner.next({ type: "callAccepted", data: data });

    }
    remoteUserHangUp(data) {
        this.onOneToOneCallListner.next({ type: "remoteUserHangUp", data: data });
    }
    callEnded() {

    }
    serverConnected() {
        console.log("OnServerConnected")
        // this.getUserInfoListener.next();
    }
    serverDisconnected() {
        console.log("OnServerDisconnected")
    }
    serverReconnecting() {

    }
    serverReconnectionFailed() {

    }
    roomRegistrationFailure() {

    }
    roomRegistrationSuccess() {
        console.log("roomRegistrationSuccess");

    }
    fetchRecentCallSuccess() { }
    fetchRecentCallFailure() { }
    readyToJoinOngoingCall() { }
    handleIncomingCallWhileBusy() { }
    handleCallRespondedFromOtherLogin() { }
    handleBrowserIncompatibility() { }
    handleVideoToggle() { }
    handleAudioToggle() { }
    handleRemoteVideoToggle() { }
    handleRemoteAudioToggle() { }
    handleScreenSharingError(error) {
        console.log("handleScreenSharingError", error);

    }
    screenShareStatusChanged(statusId, Stream) {
        console.log("screenShareStatusChanged", statusId, Stream);
        this.onRemoteStream.next({
            type: "screen",
            data: { Stream: Stream, statusId: statusId }
        });

    }
    newMemberAddedToCall() { }
    meetingCreationSuccess() { }
    meetingCreationFailure() { }
    meetingCallActivated() { }
    meetingExpired() { }
    meetingDeleted() { }
    onMeetingDeletedSuccess() { }
    onMeetingDeletedFailed() { }
    readyToJoinMeeting() { }
    remoteUserJoiningMeeting() { }
    upcomingCallsFetched() { }
    upcomingCallFetchFailed() { }
    chatRoomFetched() { }
    chatRoomFetchedFailed() { }
    messageDeliverySuccess(message) {
        console.log("Message delever", message);
        //this.onNewMessageReceived.next({ message: message, type: "message_send" });
        this.onGeneralEventEmitter.next({ message: message, type: "message_send" })

    }
    handleNewMessageReceived(message) {
        console.log("New message received", message);
        //this.onNewMessageReceived.next({ message: message, type: "message_receive" });
        this.onGeneralEventEmitter.next({ message: message, type: "message_receive" })
    }
    fetchChatListSuccess(response) {
        this.onGeneralEventEmitter.next({ type: "chatList", data: response });
    }
    fetchChatListFailed() { }
    fetchChatMessageSuccess(message) {
        this.onGeneralEventEmitter.next({ type: "conversation", data: message });
        console.log("fetchChatMessageSuccess", message);
    }
    fetchChatMessageFailed() { }
    readRoomMessageSuccess(response) {
        this.onGeneralEventEmitter.next({ type: "markReadSuccess", data: response });
    }
    readRoomMessageFailed(response) {
        this.onGeneralEventEmitter.next({ type: "markReadFail", data: response });
    }
    messageDeletionSuccess(response) { 
        this.onGeneralEventEmitter.next({ type: "deletionSuccess", data: response });
    }
    messageDeletionFailed(response) {
        this.onGeneralEventEmitter.next({ type: "deletionFailed", data: response });
     }
    messageDeletedBySender(response) {
        this.onGeneralEventEmitter.next({ type: "deletedBySender", data: response });
     }
    clearChatRoomSuccess() { }
    clearChatRoomFailed() { }
    deleteChatRoomSuccess(response) {
        this.onGeneralEventEmitter.next({ type: "deleteRoomSuccess", data: response });
    }
    deleteChatRoomFailed(response) {
        this.onGeneralEventEmitter.next({ type: "deleteRoomFailed", data: response });
    }
    userTypingInRoom(data) {
        console.log("userTypingInRoom", data);
        
     }
    sessionRefreshSuccess() { }
    sessionRefreshFailure() { }
    onParticipantJoined(data) {
        //console.log("onParticipantJoin", data);
        this.onGeneralEventEmitter.next({
            user: data,
            type: 'join'
        });
    }
    onParticipantLeft(data) {
        //console.log("onParticipantLeft", data);
        this.onGeneralEventEmitter.next({
            user: data,
            type: 'left'
        });
    }
    onParticipantRoleUpdated(data) {
        //console.log("onParticipantRoleUpdated", data);
        this.onGeneralEventEmitter.next({
            user: data,
            type: 'roleUpdate'
        });
    }
    onAudioActivity(data) {
        this.onGeneralEventEmitter.next({
            user: data,
            type: 'audioActivity'
        });

    }
    onAnalytics(err, data) {
        this.onGeneralEventEmitter.next({
            type: 'analytics',
            data: data
        });
    }
    onAdminsUpdated() { }
    onPresentersUpdated() { }
    onParticipantMuted(data) {
        //console.log("onParticipantMuted", data);
        this.onGeneralEventEmitter.next({
            user: data,
            type: 'muted'
        });

    }
    onParticipantDropped(err, data) {
        //console.log("onParticipantDropped", err, data);

    }
    onParticipantStatusUpdated(data) {
        // console.log("onParticipantStatusUpdated", data);
        this.onGeneralEventEmitter.next({
            user: data,
            type: 'change'
        });

    }
    onParticipantHandRaised(data) {
        //console.log("onParticipantHandRaised", data);
        this.onGeneralEventEmitter.next({
            user: data,
            type: 'handRaise'
        });
    }
    onParticipantHandLowered(data) {
        //console.log("onParticipantHandLowered", data);
        this.onGeneralEventEmitter.next({
            user: data,
            type: 'handLower'
        });
    }
    onMeetingEnded() {

    }
    onCallReconnected(data) {
        console.log("onCallReconnected", data);
        this.onGeneralEventEmitter.next({ type: "reconnected", data: data });

    }
    onFetchContactListSuccess(result) {
        console.log("onFetchContactListSuccess", result)
    }
    onCallReconnecting(data) {
        console.log("onCallReconnecting", data);
        this.onGeneralEventEmitter.next({ type: "reconnecting", data: data });
    }
    setYuWeehanders() {
        var userHandlers = {
            'onAccountCreateSuccess': this.accountCreateSuccess.bind(this),
            'onAccountCreateFailure': this.accountCreateFailure.bind(this),
            'onSessionCreateSuccess': this.sessionCreateSuccess.bind(this),
            'onSessionCreateFailure': this.sessionCreateFailure,
            'onCallSetupFailed': this.callSetupFailure,
            'onAllUsersOffline': this.allUsersOffline,
            'onAllUsersBusy': this.allUsersBusy,
            'onReadyToInitiateCall': this.readyToInitiateCall.bind(this),
            'onStreamRequestDowngrade': this.streamRequestDowngraded,
            'onStreamCaptureFailure': this.streamCapturefailed,
            'onLocalStreamReceived': this.onLocalStreamReceived.bind(this),
            'onIncomingCall': this.showIncomingCallNotification.bind(this),
            'onInvitationExpired': this.invitationExpired,
            'onCallHangUpSuccess': this.callHangUpSuccess.bind(this),
            'onInitiatorOffline': this.initiatorOffline,
            'onRemoteStreamReceived': this.onRemoteStreamReceived.bind(this),
            'onRemoteStreamAvailable': this.remoteStreamAvailable.bind(this),
            'onRemoteStreamStopped': this.remoteStreamStopped.bind(this),
            'onMultiRemoteStreamReceived': this.remoteScreenShared.bind(this),
            'onCallRejected': this.callRejected,
            'onCallAccepted': this.callAccepted.bind(this),
            'onRemoteCallHangUp': this.remoteUserHangUp.bind(this),
            'onCallEnded': this.callEnded,
            'onServerConnected': this.serverConnected.bind(this),
            'onServerDisconnected': this.serverDisconnected,
            'onServerReconnecting': this.serverReconnecting,
            'onServerReconnectionFailed': this.serverReconnectionFailed,
            'onRoomRegistrationFailure': this.roomRegistrationFailure,
            'onRoomRegistrationSuccess': this.roomRegistrationSuccess,
            'onFetchRecentCallSuccess': this.fetchRecentCallSuccess,
            'onFetchRecentCallFailure': this.fetchRecentCallFailure,
            // 'onReadyToJoinOngoingCall': this.readyToJoinOngoingCall,
            'onIncomingCallWhileBusy': this.handleIncomingCallWhileBusy,
            'onCallRespondedFromOtherLogin': this.handleCallRespondedFromOtherLogin,
            'onBrowserIncompatible': this.handleBrowserIncompatibility,
            'onVideoToggle': this.handleVideoToggle,
            'onAudioToggle': this.handleAudioToggle,
            'onRemoteVideoToggle': this.handleRemoteVideoToggle,
            'onRemoteAudioToggle': this.handleRemoteAudioToggle,
            'onScreenSharingError': this.handleScreenSharingError,
            'onScreenShareStatusChanged': this.screenShareStatusChanged.bind(this),
            'onNewMemberAddedToCall': this.newMemberAddedToCall,
            'onMeetingCreatedSuccessfully': this.meetingCreationSuccess,
            'onMeetingCreationFailure': this.meetingCreationFailure,
            'onMeetingCallActivated': this.meetingCallActivated,
            'onMeetingExpired': this.meetingExpired,
            'onMeetingDeleted': this.meetingDeleted,
            'onDeleteMeetingSuccess': this.onMeetingDeletedSuccess,
            'onDeleteMeetingError': this.onMeetingDeletedFailed,
            'onReadyToJoinMeeting': this.readyToJoinMeeting,
            'onRemoteUserJoiningMeeting': this.remoteUserJoiningMeeting,
            'onFetchUpcomingCallSuccess': this.upcomingCallsFetched,
            'onFetchUpcomingCallFailure': this.upcomingCallFetchFailed,
            'onFetchChatRoomSuccess': this.chatRoomFetched,
            'onFetchChatRoomFailed': this.chatRoomFetchedFailed,
            'onMessageDeliverySuccess': this.messageDeliverySuccess.bind(this),
            'onNewMessageReceived': this.handleNewMessageReceived.bind(this),
            'onFetchChatListSuccess': this.fetchChatListSuccess.bind(this),
            'onFetchChatListFailed': this.fetchChatListFailed,
            'onFetchChatMessageSuccess': this.fetchChatMessageSuccess.bind(this),
            'onFetchChatMessageFailed': this.fetchChatMessageFailed,
            'onReadMessageInRoomSuccess': this.readRoomMessageSuccess.bind(this),
            'onReadMessageInRoomFailed': this.readRoomMessageFailed.bind(this),
            'onDeleteMessageInRoomSuccess': this.messageDeletionSuccess.bind(this),
            'onDeleteMessageInRoomFailed': this.messageDeletionFailed.bind(this),
            'onMessageDeletedBySender': this.messageDeletedBySender.bind(this),
            'onClearChatRoomSuccess': this.clearChatRoomSuccess,
            'onClearChatRoomFailed': this.clearChatRoomFailed,
            'onDeleteChatRoomSuccess': this.deleteChatRoomSuccess.bind(this),
            'onDeleteChatRoomFailed': this.deleteChatRoomFailed.bind(this),
            'onUserTypingInRoom': this.userTypingInRoom.bind(this),
            'onSessionRefreshSuccess': this.sessionRefreshSuccess,
            'onSessionRefreshFailure': this.sessionRefreshFailure,
            'onParticipantJoined': this.onParticipantJoined.bind(this),
            'onParticipantLeft': this.onParticipantLeft.bind(this),
            'onAdminsUpdated': this.onAdminsUpdated,
            'onPresentersUpdated': this.onPresentersUpdated,
            'onParticipantMuted': this.onParticipantMuted.bind(this),
            'onParticipantDropped': this.onParticipantDropped.bind(this),
            'onParticipantStatusUpdated': this.onParticipantStatusUpdated.bind(this),
            'onParticipantHandRaised': this.onParticipantHandRaised.bind(this),
            'onParticipantHandLowered': this.onParticipantHandLowered.bind(this),
            'onMeetingEnded': this.onMeetingEnded,
            'onParticipantRoleUpdated': this.onParticipantRoleUpdated.bind(this),
            'onAudioActivity': this.onAudioActivity.bind(this),
            'onAnalytics': this.onAnalytics.bind(this),
            'onCallReconnecting': this.onCallReconnecting.bind(this),
            'onCallReconnected': this.onCallReconnected.bind(this),
            'onFetchContactListSuccess': this.onFetchContactListSuccess.bind(this),

        };
        this.yuWeeClient.setHandlers(userHandlers)
    }
}