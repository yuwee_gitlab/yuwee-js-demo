import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import {Config} from '../util/config';
@Injectable({
  providedIn: 'root'
})

export class UploadFileService {

  private baseUrl = Config.appUrl;

  constructor(private http: HttpClient) { }
  /*
  this.upload = async function(roomId,file,uploadType = 1){
  */
  upload(file: File, roomId: string): Observable<HttpEvent<any>> {
    const formData: FormData = new FormData();
    
    formData.append('roomid', roomId);
    formData.append('file', file, file.name);
    
    const req = new HttpRequest('POST', `${this.baseUrl}upload`, formData, {
      reportProgress: true,
      responseType: 'json'
    });

    return this.http.request(req);
  }

  getFiles(): Observable<any> {
    return this.http.get(`${this.baseUrl}/files`);
  }
}