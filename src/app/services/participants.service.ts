import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { JoinUserModel } from '../model/join.user.model';

@Injectable({ providedIn: 'root' })
export class ParticipantsService {
    participantsMap = new Map<string, JoinUserModel>();

    onParticipantsChanged = new Subject<any>();
    setParticipantMap(participantsMap){
        this.participantsMap = participantsMap;
    }
    getParticipantMap() : Map<string, JoinUserModel>{
        return this.participantsMap;
    }

}