import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Config } from '../util/config';
import {AdminModel} from '../model/admin.model';
import {LoginModel} from '../model/login.model';



@Injectable({ providedIn: "root" })
export class LoginService {
    constructor(private http: HttpClient) {

    }
    checkAccountAvailability(name: String) {
        console.log(name);
        return this.http.post<AdminModel>(Config.baseUrl + Config.checkAvailability, { "name": name })
    }
    login(data: any, _accessToken: string) {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': "Basic " + _accessToken
            })
        };
        return this.http.post<LoginModel>(Config.baseUrl + Config.individualLogin, data, httpOptions);
    }


}