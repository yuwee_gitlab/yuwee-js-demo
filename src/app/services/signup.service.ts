import { CdkNestedTreeNode } from '@angular/cdk/tree';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AdminModel } from '../model/admin.model';
import {SubcriptionPlan} from '../model/subcription.plan';
import { Config } from '../util/config';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({ providedIn: 'root' })
export class SignupService {
    constructor(private http: HttpClient) { }
    checkAccountAvailability(name: String) {
        console.log(name);
        return this.http.post<AdminModel>(Config.baseUrl + Config.checkAvailability, { "name": name })
    }
    signupIndivigualUser(data: Object, _accessToken: string) {
        return this.http.post(Config.baseUrl + Config.individualSignup, data, this.getHttpsOptions(_accessToken));
    }
    signupOtherUser(data: Object, _accessToken: string) {
        return this.http.post(Config.baseUrl + Config.otherSignup,data, this.getHttpsOptions(_accessToken));
    }
    getHttpsOptions(_accessToken: string) {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': "Basic " + _accessToken
            })
        };
        return httpOptions;
    }
    getSubscriptionPlans(){
        var getUrl = Config.baseUrl + Config.getSubscriptionPlans + '?accountType=integrate' ;
        return this.http.get<SubcriptionPlan>(getUrl);
    }

} 