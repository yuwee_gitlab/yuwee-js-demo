import { ElementRef, Injectable, Renderer2, RendererFactory2 } from '@angular/core';
import * as CryptoJS from 'crypto-js';
import { Config } from '../util/config';
@Injectable({ providedIn: 'root' })
export class CommonService {

    private renderer: Renderer2;

    constructor(private rendererFactory2: RendererFactory2) {
        this.renderer = rendererFactory2.createRenderer(null, null)
    }
    isLoggedIn() {
        var userData = localStorage.getItem("yuvitimeStorage");
        var data;
        if (userData) {
            data = JSON.parse(userData);
        }
        if (data && (data.authToken && data.authToken !== "null" || data.access_token && data.access_token !== "null")) {
            return true;
        } else {
            return false;
        }
    }
    showElement(shareVideoDiv: ElementRef<HTMLDivElement>) {
        shareVideoDiv.nativeElement.setAttribute('style', 'display: block;');

    }

    addElemetent(parentElement: HTMLDivElement) {
        const div = this.renderer.createElement('div');
        div.setAttribute('style', 'background: white; width: 200px; height:200px; border: 1px solid red; margin:5px;');
        /*  this.renderer.setStyle(
             div.nativeElement,
             'wid',
             '2px dashed olive'
           ); */
        this.renderer.appendChild(parentElement, div)
    }
    getColumns(count: number) {
        if (count === 1) {
            return 1;
        }
        var columns = 2;
        for (var index = 2; index <= 5; index++) {
            if (this.checkColumnBetween(count, index)) {
                columns = index;
                break;
            }
        }
        return columns;
    }
    checkColumnBetween(count: number, column: number): boolean {
        var square = column * column;
        var squareOneLess = (column - 1) * (column - 1);
        if (count >= (squareOneLess + 1) && count <= square) {
            return true;
        } else {
            return false;
        }
    }

    getRowCount(count: number): number {
        if (count >= 1 && count <= 2) {
            return 1;
        } else if (count >= 3 && count <= 6) {
            return 2;
        } else if (count >= 7 && count <= 12) {
            return 3;
        } else if (count >= 13 && count <= 20) {
            return 4;
        } else if (count >= 16 && count <= 25) {
            return 5;
        }

    }


    encrypt(value) {
        var key = CryptoJS.enc.Utf8.parse(Config.keys);
        var iv = CryptoJS.enc.Utf8.parse(Config.keys);
        var encrypted = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(value.toString()), key,
            {
                keySize: 16,
                iv: iv,
                mode: CryptoJS.mode.CBC,
                padding: CryptoJS.pad.Pkcs7
            });

        return encrypted.toString();
    }

    //The get method is use for decrypt the value.
    decrypt(value) {
        var key = CryptoJS.enc.Utf8.parse(Config.keys);
        var iv = CryptoJS.enc.Utf8.parse(Config.keys);
        var decrypted = CryptoJS.AES.decrypt(value, key, {
            keySize: 16,
            iv: iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        });

        return decrypted.toString(CryptoJS.enc.Utf8);
    }
    getName(conversation) {
        if (conversation.groupInfo) {
            return conversation.groupInfo.name;
        } else if (conversation.name) {
            /*  if(conversation.membersInfo[0]._id !== this.localUser.user._id){
               return conversation.membersInfo[0].name;
             }else if(conversation.membersInfo[1]._id !== this.localUser.user._id){
               return conversation.membersInfo[1].name;
             } else{
               return conversation.membersInfo[0].name;
             } */
            return conversation.name;

        } else if (conversation.lastMessageByName) {
            return conversation.lastMessageByName.name;
        } else {
            return "";
        }
    }
}